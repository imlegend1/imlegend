const COMMAND_DELAY: number = 1000;
const actions: string[] = ['visit', 'click', 'trigger', 'type', 'clear', 'reload', 'contains'];

for (let i: number = 0; i < actions.length; i += 1)
{
  const command: string = actions[i];

  Cypress.Commands.overwrite(command, (originalFn: (...data: Array<unknown>) => void, ...args: Array<unknown>): Promise<unknown> =>
  {
    const origVal: unknown = originalFn(...(args as Array<unknown>));

    return new Promise((resolve: (data: unknown) => void) =>
    {
      setTimeout(() => 
      {
        resolve(origVal);
      }, COMMAND_DELAY);
    });
  });
}
