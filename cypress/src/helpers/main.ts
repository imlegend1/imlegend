import { frontendHost } from '../constants/host';

/**
 * @description В этом хелпере сосредоточен функционал, необходимый для запуска на любой странице
 */
export class MainHelper
{
  public static beforeEachRunner(): void
  {
    cy.server();
    cy.viewport('macbook-16');
    cy.visit(frontendHost);
  };
}
