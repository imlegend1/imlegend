import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthStrategy } from '../auth/auth.strategy';
import { GetUserEntity } from '../auth/get-user.entity.decorator';
import { UserEntity } from '../auth/user.entity';

@UseGuards(AuthGuard(AuthStrategy.ILJwt))
@Controller('tasks')
export class TaskController
{
	@Get()
  public async get(@GetUserEntity() userEntity: UserEntity): Promise<string>
  {
  	console.log(userEntity);
    return 'GET!';
  }

	@Post()
	public async post(): Promise<string>
	{
	  return 'POST!';
	}
}
