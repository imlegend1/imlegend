import { EntityRepository, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { UserEntity } from './user.entity';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { UserErrorCodes } from './user.error-codes';

/**
 * @description Объекты этого класса делают чтение и запись из базы
 */
@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity>
{
  /**
	 * @description Шифрует входящие данные
	 * @param password Исходный текст пароля
	 * @param salt Ключ шифрования
	 * @return Зашифрованный пароль для сохранения в базу
	 */
  private static Hash(password: string, salt: string): Promise<string>
  {
    return bcrypt.hash(password, salt);
  }

  /**
	 * @return Рандомный ключ
	 */
  private static Salt(): Promise<string>
  {
    return bcrypt.genSalt();
  }

  /**
	 * @description Создает TypeORM сущность из DTO
	 * @param username Имя, которое будет записано в сущность
	 * @param password Пароль, который будет использован для записи в сущность
	 * @return Сущность, которая работает с подключением к базе данных
	 */
  private static async FromDTO({ username, password }: AuthCredentialsDTO): Promise<UserEntity>
  {
    const user: UserEntity = new UserEntity();

    user.name = username;
    user.salt = await UserRepository.Salt();
    user.password = await UserRepository.Hash(password, user.salt);

    return user;
  }

  /**
	 * @description Проверяет логин и пароль пользователя на совпадение с образцами
	 * @return true, если сохраненные в базе данные совпали с образцами
	 */
  public async validate({ username, password }: AuthCredentialsDTO): Promise<boolean>
  {
    const user: UserEntity = await this.findOne({ name: username });
    const hashedPassword: string = await UserRepository.Hash(password, user.salt);

    return (hashedPassword === user?.password);
  }

  public async signUp(authCredentialsDTO: AuthCredentialsDTO): Promise<void>
  {
    const user: UserEntity = await UserRepository.FromDTO(authCredentialsDTO);

    try
    {
      await user.save();
    }
    catch (e)
    {
      if (e.code === UserErrorCodes.DuplicatedUser)
      {
        throw new ConflictException(`User ${authCredentialsDTO.username} is already exists.`);
      }

      throw new InternalServerErrorException();
    }
  }
}
