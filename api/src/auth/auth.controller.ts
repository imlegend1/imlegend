import { Body, Controller, Post } from '@nestjs/common';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { AccessTokenDTO } from './dto/access-token.dto';

@Controller('auth')
export class AuthController
{
  constructor(private authService: AuthService)
  {}

	@Post('/signup')
  public async signUp(@Body() authCredentialsDTO: AuthCredentialsDTO): Promise<void>
  {
    return this.authService.signUp(authCredentialsDTO);
  }

	@Post('/signin')
	public async signIn(@Body() authCredentialsDTO: AuthCredentialsDTO): Promise<AccessTokenDTO>
	{
	  return this.authService.signIn(authCredentialsDTO);
	}
}
