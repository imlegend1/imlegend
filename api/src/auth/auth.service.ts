import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { UserRepository } from './user.repository';
import { AccessTokenDTO } from './dto/access-token.dto';

/**
 * @description Сервис для обслуживания соответствующего контроллера
 */
@Injectable()
export class AuthService
{
  /**
	 * @param userRepository Объект, непосредственно выполняющий запись в базу
	 * @param jwtService Сервис Nest.js, нужен для создания JWT
	 */
  constructor(
		@InjectRepository(UserRepository)
		private userRepository: UserRepository,
		private jwtService: JwtService,
  )
  {}

  /**
	 * @description Выполняет все действия, связанные с добавлением нового пользователя в базу данных
	 * @param authCredentialsDTO Данные потенциального нового пользователя
	 */
  public async signUp(authCredentialsDTO: AuthCredentialsDTO): Promise<void>
  {
    return this.userRepository.signUp(authCredentialsDTO);
  }

  /**
	 * @description Создает в глобальном состоянии Nest.js токен, ассоциированный с пользователем, и возвращает его
	 */
  public async signIn(authCredentialsDTO: AuthCredentialsDTO): Promise<AccessTokenDTO>
  {
    if (!await this.userRepository.validate(authCredentialsDTO))
    {
      throw new UnauthorizedException('Invalid credentials');
    }

    const accessToken: string = this.jwtService.sign({ username: authCredentialsDTO.username });

    return { accessToken };
  }
}
