import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtPayload } from './jwt.payload';
import { UserRepository } from './user.repository';
import { UserEntity } from './user.entity';
import { AuthStrategy } from './auth.strategy';

/**
 * @description Этот сервис используется AuthGuard'ами, при успешной валидации Nest.js передает управление запросом контроллеру
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, AuthStrategy.ILJwt)
{
  constructor(
		@InjectRepository(UserRepository)
		private userRepository: UserRepository,
  )
  {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  public async validate({ username }: JwtPayload): Promise<UserEntity>
  {
    const user: UserEntity = await this.userRepository.findOne({ name: username });

    if (!user)
    {
      throw new UnauthorizedException();
    }

    return user;
  }
}
