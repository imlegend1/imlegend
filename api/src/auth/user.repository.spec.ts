import { Test, TestingModule } from '@nestjs/testing';
import * as bcrypt from 'bcrypt';
import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { UserRepository } from './user.repository';
import * as userEntityModule from './user.entity';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { UserEntity } from './user.entity';
import { UserErrorCodes } from './user.error-codes';

jest.mock('bcrypt');
jest.mock('./user.entity');

describe('UserRepository', (): void =>
{
  const mockAuthCredentialsDTO: AuthCredentialsDTO = {
    username: 'BoyFromBryansk',
    password: 'NaRayone123',
  };
  const mockHashedPassword: string = 'bCrypt__HasheD__pass';
  const mockRandomHash: string = 'z1Q2ed3l2KJjrS1J';
  const userSaveSpy = jest.fn();

  let repository: UserRepository;

  beforeEach(async (): Promise<void> =>
  {
    (userEntityModule.UserEntity as unknown as jest.Mock).mockReturnValue(
      {
        name: '',
        password: '',
        salt: '',
        save: userSaveSpy,
      }
    );

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserRepository,
      ],
    }).compile();

    repository = module.get<UserRepository>(UserRepository);
  });

  afterEach((): void =>
  {
    jest.resetAllMocks();
    jest.restoreAllMocks();
  });

  it('should be created', (): void =>
  {
    expect(repository).not.toBeNull();
  });

  describe('Hash', (): void =>
  {
    beforeEach((): void =>
    {
      bcrypt.hash.mockResolvedValue(mockHashedPassword);
    });

    it('should resolve hashed string', (): void =>
    {
      expect(UserRepository['Hash']('Qwerty123', 'someSalt')).resolves.toEqual(mockHashedPassword);
    });
  });

  describe('Salt', (): void =>
  {
    beforeEach((): void =>
    {
      bcrypt.genSalt.mockResolvedValue(mockRandomHash);
    });

    it('should generate random hash', (): void =>
    {
      expect(UserRepository['Salt']()).resolves.toEqual(mockRandomHash);
    });
  });

  describe('FromDTO', (): void =>
  {
    it('should create User instance', async (): Promise<void> =>
    {
      await UserRepository['FromDTO'](mockAuthCredentialsDTO);

      expect(userEntityModule.UserEntity).toHaveBeenCalledTimes(1);
    });

    it('should write name of user in nem field', async (): Promise<void> =>
    {
      const result: UserEntity = await UserRepository['FromDTO'](mockAuthCredentialsDTO);

      expect(result.name).toEqual(mockAuthCredentialsDTO.username);
    });

    it('should write random key as salt', async (): Promise<void> =>
    {
      bcrypt.genSalt.mockResolvedValue(mockRandomHash);

      const result: UserEntity = await UserRepository['FromDTO'](mockAuthCredentialsDTO);

      expect(result.salt).toEqual(mockRandomHash);
    });

    it('should write hashed user password', async (): Promise<void> =>
    {
      bcrypt.hash.mockResolvedValue(mockHashedPassword);

      const result: UserEntity = await UserRepository['FromDTO'](mockAuthCredentialsDTO);

      expect(result.password).toEqual(mockHashedPassword);
    });
  });

  describe('signUp', (): void =>
  {
    beforeEach((): void =>
    {
      jest.spyOn(UserRepository, 'FromDTO' as unknown as never);
      bcrypt.genSalt.mockResolvedValue(mockRandomHash);
      bcrypt.hash.mockResolvedValue(mockHashedPassword);
    });

    it('should create User instance', async (): Promise<void> =>
    {
      await repository.signUp(mockAuthCredentialsDTO);

      expect(userEntityModule.UserEntity).toHaveBeenCalledTimes(1);
    });

    it('should fill user with data', async (): Promise<void> =>
    {
      await repository.signUp(mockAuthCredentialsDTO);

      expect(UserRepository['FromDTO']).toHaveBeenCalledTimes(1);
      expect(UserRepository['FromDTO']).toHaveBeenCalledWith(mockAuthCredentialsDTO);
    });

    it('should save user with date after fill', async (): Promise<void> =>
    {
      await repository.signUp(mockAuthCredentialsDTO);

      expect(userSaveSpy).toHaveBeenCalledTimes(1);
      expect(userSaveSpy.mock.instances[0].name).toEqual(mockAuthCredentialsDTO.username);
      expect(userSaveSpy.mock.instances[0].password).toEqual(mockHashedPassword);
      expect(userSaveSpy.mock.instances[0].salt).toEqual(mockRandomHash);
    });

    it('should reject promise with internal error by default error from save', (): void =>
    {
      (userEntityModule.UserEntity as unknown as jest.Mock).mockReturnValue(
        {
          name: '',
          password: '',
          salt: '',
          save: () => Promise.reject({}),
        }
      );

      expect(repository.signUp(mockAuthCredentialsDTO)).rejects.toThrow(new InternalServerErrorException());
    });

    it('should reject promise with conflict error with message if user duplicates', (): void =>
    {
      (userEntityModule.UserEntity as unknown as jest.Mock).mockReturnValue(
        {
          name: '',
          password: '',
          salt: '',
          save: () => Promise.reject({ code: UserErrorCodes.DuplicatedUser }),
        }
      );

      expect(repository.signUp(mockAuthCredentialsDTO)).rejects.toThrow(
        new ConflictException(`User ${mockAuthCredentialsDTO.username} is already exists.`)
      );
    });
  });
});
