import { BaseEntity, Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity()
@Unique(['name'])
export class UserEntity extends BaseEntity
{
	/**
	 * @description Уникальный номер пользователя
	 */
	@PrimaryGeneratedColumn()
	public id: number;

	@Column()
	public name: string;

	/**
	 * @description Пароль пользователя. Хранится в зашифрованном виде
	 */
	@Column()
	public password: string;

	/**
	 * @description Ключ для шифровки пароля
	 */
	@Column()
	public salt: string;
}
