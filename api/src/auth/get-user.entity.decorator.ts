import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserEntity } from './user.entity';

type Request = { user: UserEntity };

export const GetUserEntity = createParamDecorator((_: null, ctx: ExecutionContext): UserEntity =>
{
  const req: Request = ctx.switchToHttp().getRequest<Request>();
  return req.user;
});