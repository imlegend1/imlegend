/**
 * @description Структура, на основе которой составляется JWT
 */
export interface JwtPayload
{
	username: string;
}
