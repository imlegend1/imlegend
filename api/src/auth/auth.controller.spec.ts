import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';

describe('AuthController', () =>
{
  const authCredentialsDTO: AuthCredentialsDTO = { username: 'NewUser', password: 'qwerty123' };
  let controller: AuthController;

  beforeEach(async () =>
  {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            signUp: jest.fn(async () => undefined),
            signIn: jest.fn(async () => undefined),
          }
        }
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  describe('signUp', (): void =>
  {
    afterEach((): void =>
    {
      jest.resetAllMocks();
    });

    it('should resolve if service resolves', (): void =>
    {
      (controller['authService'].signUp as jest.Mock).mockImplementation(() => Promise.resolve());

      expect(controller.signUp(authCredentialsDTO)).resolves.toEqual(undefined);
    });

    it('should reject if service rejects', (): void =>
    {
      (controller['authService'].signUp as jest.Mock).mockImplementation(() => Promise.reject('Error!'));

      expect(controller.signUp(authCredentialsDTO)).rejects.toEqual('Error!');
    });
  });

  describe('signIn', (): void =>
  {
    afterEach((): void =>
    {
      jest.resetAllMocks();
    });

    it('should resolve if service resolves', (): void =>
    {
      (controller['authService'].signIn as jest.Mock).mockImplementation(() => Promise.resolve('Response'));

      expect(controller.signIn(authCredentialsDTO)).resolves.toEqual('Response');
    });

    it('should reject if service rejects', (): void =>
    {
      (controller['authService'].signIn as jest.Mock).mockImplementation(() => Promise.reject('Error!'));

      expect(controller.signIn(authCredentialsDTO)).rejects.toEqual('Error!');
    });
  });
});
