import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { UserRepository } from './user.repository';

describe('AuthService', () =>
{
  const authCredentialsDTO: AuthCredentialsDTO = { username: 'NewUser', password: 'qwerty123' };
  let service: AuthService;

  beforeEach(async () =>
  {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UserRepository,
          useValue: {
            signUp: jest.fn(),
          },
        },
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn(),
          }
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  describe('signUp', (): void =>
  {
    afterEach((): void =>
    {
      jest.resetAllMocks();
    });

    it('should resolve if repository resolves', (): void =>
    {
      (service['userRepository'].signUp as jest.Mock).mockImplementation(() => Promise.resolve());

      expect(service.signUp(authCredentialsDTO)).resolves.toEqual(undefined);
    });

    it('should reject if repository rejects', (): void =>
    {
      (service['userRepository'].signUp as jest.Mock).mockImplementation(() => Promise.reject('Error!'));

      expect(service.signUp(authCredentialsDTO)).rejects.toEqual('Error!');
    });
  });
});
