import React from 'react';
import { storesContext } from '../contexts';
import rootStore from '../stores/root-store';

type RootStore = (typeof rootStore);

export const useStores = (): RootStore => React.useContext(storesContext);
