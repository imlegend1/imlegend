import { createContext } from 'react';
import rootStore from '../stores/root-store';

export const storesContext = createContext(rootStore);
