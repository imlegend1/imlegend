import authStore from './auth-store';

const rootStore = {
  authStore
};

export default rootStore;
