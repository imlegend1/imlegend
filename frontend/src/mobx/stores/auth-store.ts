import { action, computed, observable } from 'mobx';
import { Maybe } from '../../../interfaces/structs/maybe';

interface User
{
	login: string;
}

class AuthStore
{
	@observable
	private user_: Maybe<User>;

	@computed
	public get user(): Maybe<User>
	{
		return this.user_;
	}

	@computed
	public get isAuthorized(): boolean
	{
	  return Boolean(this.user_?.login);
	}

	@action
	public async authenticate(): Promise<void>
	{
	  this.user_ = {
	  	login: 'Rehten765',
	  };
	}
}

const authStore: AuthStore = new AuthStore();

export default authStore;
