import axios from 'axios';
import apiService from './api-service';
import { Benchmark } from '../../test-utils/benchmark';
import { repeat } from '../../test-utils/repeat';
import { Benchmarks } from '../../test-utils/benchmark.constants';

jest.mock('axios');

describe('ApiService', () =>
{
  /**
	 * @description Проверяет, возвращает ли метод apiService полученное значение при выполнении запроса
	 * @param method Функция-член класса
	 * @param data Данные запроса, либо id
=	 */
  const testResolve = async <T>(method: string, data?: T): Promise<void> =>
  {
    // @ts-ignore
    jest.spyOn(apiService, 'request');
    // @ts-ignore
    apiService['request'].mockResolvedValue({ test: 'MyTest' });

    // @ts-ignore
    if ((method in apiService) && (apiService[method] instanceof Function))
    {
      // @ts-ignore
      await expect(apiService[method]('test', data)).resolves.toEqual({ test: 'MyTest' });
    }
    else
    {
      throw new Error(`${method} is not exist in apiService`);
    }
  };
  /**
	 * @description Проверяет передачу ошибки при ошибочном завершении запроса в некотором метода apiService
	 * @param method Функция-член класса
	 * @param data Данные запроса, либо id
	 */
  const testReject = async <T>(method: string, data?: T): Promise<void> =>
  {
    // @ts-ignore
    jest.spyOn(apiService, 'request');
    // @ts-ignore
    apiService['request'].mockRejectedValue({ error: 500 });

    // @ts-ignore
    if ((method in apiService) && (apiService[method] instanceof Function))
    {
      // @ts-ignore
      await expect(apiService[method]('test', data)).rejects.toEqual({ error: 500 });
    }
    else
    {
      throw new Error(`${method} is not exist in apiService`);
    }
  };

  describe('ApiService | query', () =>
  {
    it('should return empty string on undefined query struct', () =>
    {
      expect(apiService['query'](undefined)).toEqual('');
    });

    it('should return empty string on empty object', () =>
    {
      expect(apiService['query']({})).toEqual('');
    });

    it('should return query string on filled object', () =>
    {
      expect(apiService['query']({ a: '1', b: 'true' })).toEqual('?a=1&b=true');
    });

    it('should be immediately fast', () =>
    {
      const benchmark: Benchmark = new Benchmark();

      benchmark.start();
      repeat(() =>
      {
        apiService['query']({ a: '1', b: 'true' });
      });
      benchmark.end();

      expect(benchmark.time()).toBeLessThanOrEqual(Benchmarks.Immediate);
    });
  });

  describe('ApiService | getById', () =>
  {
    afterEach(() =>
    {
      jest.restoreAllMocks();
    });

    it('should call request with correct id', async () =>
    {
      // @ts-ignore
      jest.spyOn(apiService, 'request');
      // @ts-ignore
      apiService['request'].mockResolvedValue({ test: 'MyTest' });

      await apiService.getById('/test', '123');

      expect(apiService['request']).toHaveBeenCalledTimes(1);
      expect(apiService['request']).toHaveBeenCalledWith('get', '/test/123');
    });

    it('should resolve response if request resolved', async () =>
    {
      await testResolve<string>('getById', '123');
    });

    it('should reject response if request rejected', async () =>
    {
      await testReject<string>('getById', '123');
    });
  });

  describe('ApiService | get', () =>
  {
    afterEach(() =>
    {
      jest.restoreAllMocks();
    });

    it('should call request same url', async () =>
    {
      // @ts-ignore
      jest.spyOn(apiService, 'request');
      // @ts-ignore
      apiService['request'].mockResolvedValue({ test: 'MyTest' });

      await apiService.get('/test');

      expect(apiService['request']).toHaveBeenCalledTimes(1);
      expect(apiService['request']).toHaveBeenCalledWith('get', '/test');
    });

    it('should call request with url with query', async () =>
    {
      // @ts-ignore
      jest.spyOn(apiService, 'request');
      // @ts-ignore
      apiService['request'].mockResolvedValue({ test: 'MyTest' });

      await apiService.get('/test', { a: '1', b: 'true' });

      expect(apiService['request']).toHaveBeenCalledTimes(1);
      expect(apiService['request']).toHaveBeenCalledWith('get', '/test?a=1&b=true');
    });

    it('should resolve response if request resolved', async () =>
    {
      await testResolve<string>('get');
    });

    it('should reject response if request rejected', async () =>
    {
      await testReject<string>('get');
    });
  });

  describe('ApiService | post', () =>
  {
    afterEach(() =>
    {
      jest.restoreAllMocks();
    });

    it('should call request with same url and payload', async () =>
    {
      // @ts-ignore
      jest.spyOn(apiService, 'request');
      // @ts-ignore
      apiService['request'].mockResolvedValue({ test: 'MyTest' });

      await apiService.post('/test', { payloaded: 123 });

      expect(apiService['request']).toHaveBeenCalledTimes(1);
      expect(apiService['request']).toHaveBeenCalledWith('post', '/test', { payloaded: 123 });
    });

    it('should resolve response if request resolved', async () =>
    {
      await testResolve<Record<string, number>>('post', { a: 1 });
    });

    it('should reject response if request rejected', async () =>
    {
      await testReject<Record<string, number>>('post', { a: 1 });
    });
  });

  describe('ApiService | put', () =>
  {
    afterEach(() =>
    {
      jest.restoreAllMocks();
    });

    it('should call request with same url and payload', async () =>
    {
      // @ts-ignore
      jest.spyOn(apiService, 'request');
      // @ts-ignore
      apiService['request'].mockResolvedValue({ test: 'MyTest' });

      await apiService.put('/test', { payloaded: 123 });

      expect(apiService['request']).toHaveBeenCalledTimes(1);
      expect(apiService['request']).toHaveBeenCalledWith('put', '/test', { payloaded: 123 });
    });

    it('should resolve response if request resolved', async () =>
    {
      await testResolve<Record<string, number>>('put', { a: 1 });
    });

    it('should reject response if request rejected', async () =>
    {
      await testReject<Record<string, number>>('put', { a: 1 });
    });
  });

  describe('ApiService | delete', () =>
  {
    afterEach(() =>
    {
      jest.restoreAllMocks();
    });

    it('should call request with same url and payload', async () =>
    {
      // @ts-ignore
      jest.spyOn(apiService, 'request');
      // @ts-ignore
      apiService['request'].mockResolvedValue({ test: 'MyTest' });

      await apiService.delete('/test', '123');

      expect(apiService['request']).toHaveBeenCalledTimes(1);
      expect(apiService['request']).toHaveBeenCalledWith('delete', '/test/123');
    });

    it('should resolve response if request resolved', async () =>
    {
      await testResolve<string>('delete', '123');
    });

    it('should reject response if request rejected', async () =>
    {
      await testReject<string>('delete', '123');
    });
  });

  describe('ApiService | request', () =>
  {
    beforeEach(() =>
    {
      // @ts-ignore
      axios.request.mockResolvedValue({ mojo: true });
    });

    afterEach(() =>
    {
      jest.restoreAllMocks();
    });

    it('should resolve response from axios', async () =>
    {
      await expect(apiService['request']('get', '/test')).resolves.toEqual({ mojo: true });
    });

    it('should call axios request with same data', async () =>
    {
      await apiService['request']('put', '/test', { mojo: false });

      expect(axios.request).toHaveBeenCalledWith({
        method: 'put',
        url: '/test',
        data: {
          mojo: false,
        },
      });
    });
  });
});
