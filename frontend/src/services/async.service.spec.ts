import asyncService from './async.service';

describe('AsyncService', () =>
{
  describe('AsyncService | retryAfter', () =>
  {
    const mainCallbackResolved = jest.fn((value: string): Promise<string> => Promise.resolve(value));
    const mainCallbackRejected = jest.fn((): Promise<string> => Promise.reject(new Error('Promise rejected')));

    const altCallbackResolved = jest.fn((value: string): Promise<string> => Promise.resolve(value));
    const altCallbackRejected = jest.fn((): Promise<string> => Promise.reject(new Error('Promise rejected')));

    afterEach(() =>
    {
      jest.clearAllMocks();
    });

    it('should call main action', async () =>
    {
      await asyncService.retryAfter(() => mainCallbackResolved('Simple Resolve'), altCallbackRejected);

      expect(mainCallbackResolved).toHaveBeenCalledTimes(1);
    });

    it('should resolve if main action resolved', async () =>
    {
      await expect(
        asyncService.retryAfter(
          () => mainCallbackResolved('Main Resolve Alt Reject'),
          altCallbackRejected,
        )
      ).resolves.toEqual('Main Resolve Alt Reject');
    });

    it('should call alt action if main action rejected', async () =>
    {
      await asyncService.retryAfter(mainCallbackRejected, altCallbackRejected).catch(() => true);

      expect(altCallbackRejected).toHaveBeenCalledTimes(1);
    });

    it('should reject if alt action reject', async () =>
    {
      await expect(asyncService.retryAfter(mainCallbackRejected, altCallbackRejected)).rejects.toThrow(Error);
    });

    it('should repeat main action call if alt action resolve', async () =>
    {
      await asyncService.retryAfter(
        mainCallbackRejected,
        () => altCallbackResolved('Token:123456'),
      ).catch(() => true);

      expect(mainCallbackRejected).toHaveBeenCalledTimes(2);
    });

    it('should resolve if main action second call is resolved', async () =>
    {
      let increment: number = 0;
      const mainAction = jest.fn((): Promise<string> =>
      {
        if (increment)
        {
          return mainCallbackResolved('Second Call');
        }

        increment += 1;
        return mainCallbackRejected();
      });
      const altAction = (): Promise<string> => altCallbackResolved('Token:987654321');

      await expect(asyncService.retryAfter(mainAction, altAction)).resolves.toEqual('Second Call');

      expect(mainAction).toHaveBeenCalledTimes(2);
    });
  });
});
