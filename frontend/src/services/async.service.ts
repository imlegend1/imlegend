/**
 * @description Класс для работы со сложными асинхронными действиями
 */
class AsyncService
{
  /**
	 * @description Заставляет первую функцию при выбросе исключения повторить выполнение, после вызова второй функции
	 * @param mainCallback Основное действие, метод возвращает вызов именно этой функции
	 * @param altCallback Альтернативное действие, предположительно решающее проблему, из-за которой первое действие завершилось с ошибкой
	 */
  public async retryAfter<T, R>(mainCallback: () => Promise<T>, altCallback: () => Promise<R>): Promise<T>
  {
    try
    {
      return await mainCallback();
    }
    catch (e)
    {
      await altCallback();

      return mainCallback();
    }
  }
}

const asyncService: AsyncService = new AsyncService();

export default asyncService;
