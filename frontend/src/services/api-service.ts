import axios, { Method } from 'axios';


class ApiService
{
	private readonly axios = axios;

	private query(params: Record<string, string> | undefined): string
	{
	  if (!params || !Object.entries(params).length)
	  {
	    return '';
	  }

	  const queryParams: [string, string][] = Object.entries(params);
	  const paramsLink: string = queryParams
	    .map(([key, value]: [string, string], index: number, array: [string, string][]) =>
	    {
	      switch (index)
	      {
	      case 0:
	        return `?${key}=${value}&`;
	      case array.length - 1:
	        return `${key}=${value}`;
	      default:
	        return `${key}=${value}&`;
	      }
	    })
	    .reduce((prev: string, cur: string): string => prev + cur);

	  return paramsLink;
	}

	private request<I, O>(method: Method, url: string, payload?: I): Promise<O>
	{
	  return this.axios.request({ method, url, data: payload });
	}

	public get<T>(url: string, params?: Record<string, string>): Promise<T>
	{
	  return this.request('get', url + this.query(params));
	}

	public getById<T>(url: string, id: string): Promise<T>
	{
	  return this.request('get', `${url}/${id}`);
	}

	public put<I, O>(url: string, payload: I): Promise<O>
	{
	  return this.request('put', url, payload);
	}

	public post<I, O>(url: string, payload: I): Promise<O>
	{
	  return this.request('post', url, payload);
	}

	public delete(url: string, id: string): Promise<void>
	{
	  return this.request('delete', `${url}/${id}`);
	}
}

const apiService: ApiService = new ApiService();

export default apiService;
