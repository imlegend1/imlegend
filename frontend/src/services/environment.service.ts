/**
 * @description Класс для определения среды выполнения кода(браузер или сервер), а так же ее параметров
 */
class EnvironmentService
{
  public isBrowser(): boolean
  {
    return (typeof window !== 'undefined');
  }

  public isServer(): boolean
  {
    return !this.isBrowser();
  }
}

const environmentService: EnvironmentService = new EnvironmentService();

export default environmentService;
