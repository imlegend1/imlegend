import { Control } from '../../../interfaces/forms/control';

export interface ILFieldProps<T> extends Control<T>
{
	name: string;
	/**
	 * @description Значение false означает, что пользователь не взаимодействовал с данным контролом.
	 */
	touched?: boolean;
}
