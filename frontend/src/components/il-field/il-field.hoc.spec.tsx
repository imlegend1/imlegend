import React  from 'react';
import { act, Simulate, SyntheticEventData } from 'react-dom/test-utils';
import { Form, Formik } from 'formik';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';
import { ILInputField } from '../il-input/il-input.field';

describe('ILFieldHOC', (): void =>
{
  let sandboxRenderer: SandboxRenderer;

  const TestComponent = (props: { spy?: jest.Mock }): React.ReactElement =>
  {
    return (
      <Formik
        initialValues={{test: 'Abibasta'}}
        validate={(_: { test: string }): object => ({ test: 'Error!' } as object)}
        validateOnMount
        validateOnChange
        onSubmit={(): void => undefined}
      >
        {
          (): React.ReactElement => (
            <Form>
              <ILInputField
                name="test"
                onChange={props?.spy}
              />
            </Form>
          )
        }
      </Formik>
    );
  };

  const input = (): HTMLInputElement => sandboxRenderer.container.querySelector('input.il-input') as HTMLInputElement;
  const form = (): HTMLFormElement => sandboxRenderer.container.querySelector('form') as HTMLFormElement;

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;
  });

  it('should render input element', () =>
  {
    sandboxRenderer = new SandboxRenderer(<TestComponent />);

    expect(input()).not.toBeNull();
  });

  it('should render input with value', () =>
  {
    sandboxRenderer = new SandboxRenderer(<TestComponent />);

    expect(input().value).toEqual('Abibasta');
  });

  it('should change value after same event', () =>
  {
    sandboxRenderer = new SandboxRenderer(<TestComponent />);

    Simulate.change(input(), { target: { value: '123' } as unknown } as SyntheticEventData);

    expect(input().value).toEqual('123');
  });

  it('should render input as invalid', async () =>
  {
    sandboxRenderer = new SandboxRenderer(<TestComponent />);

    await act(async (): Promise<void> =>
    {
      Simulate.focus(input());
      Simulate.change(input(), { target: { value: '123' } as unknown } as SyntheticEventData);
      Simulate.blur(input());
      Simulate.submit(form());
    });

    expect(input().className).toMatch('il-input--invalid');
  });

  it('should call onChange', async () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<TestComponent spy={spy} />);

    await act(async (): Promise<void> =>
    {
      Simulate.change(input(), { target: { value: '123' } as unknown } as SyntheticEventData);
    });

    expect(spy).toHaveBeenCalled();
  });
});
