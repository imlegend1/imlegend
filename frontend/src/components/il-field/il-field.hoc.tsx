import React from 'react';
import { useField } from 'formik';
import { ILFieldProps } from './il-field.props';

export const ILFieldHOC = <T extends ILFieldProps<R>, R>(Component: React.FC<T>): React.FC<T> =>
{
  return (props: T): React.ReactElement =>
  {
    const { name, onChange }: T = props;

    const [field, meta, helpers] = useField(name);

    const { value, onBlur } = field;
    const { error, touched } = meta;
    const { setValue } = helpers;

    const onFieldBlur = (): void =>
    {
      // Эта переменная передает Formik Field имя целевого поля.
      const eventInfo: object = { target: { name } };

      onBlur(eventInfo);
    };
    const onFieldChange = (value: R): void =>
    {
      setValue(value);

      if (onChange)
      {
        onChange(value);
      }
    };

    return (
      <Component
        {...props}
        value={value}
        invalid={touched && error}
        onBlur={onFieldBlur}
        onChange={onFieldChange}
      />
    );
  };
};
