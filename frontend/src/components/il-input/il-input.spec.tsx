import React from 'react';
import { Simulate, SyntheticEventData } from 'react-dom/test-utils';
import { ILInput } from './il-input';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';
import { ILInputTypes } from './il-input.types';

describe('ILInput', (): void =>
{
  let sandboxRenderer: SandboxRenderer;

  const input = (): HTMLInputElement => sandboxRenderer.container.querySelector('input.il-input') as HTMLInputElement;

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;
  });

  it('should render input element', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILInput />);

    expect(input()).not.toBeNull();
  });

  it('should render input with text type by default', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILInput />);

    expect(input().type).toEqual(ILInputTypes.Text);
  });

  it('should render input with password type', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILInput type={ILInputTypes.Password} />);

    expect(input().type).toEqual(ILInputTypes.Password);
  });

  it('should render input with value', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILInput value="123" />);

    expect(input().value).toEqual('123');
  });

  it('should output new value as onChange callback', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILInput onChange={spy} />);

    Simulate.change(input(), { target: { value: '123' } as unknown } as SyntheticEventData);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('123');
  });

  it('should render disabled props as readonly input', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILInput disabled />);

    expect(input().readOnly).toBeTruthy();
  });

  it('should add invalid class if component rendered as invalid', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILInput invalid />);

    expect(input().className).toMatch('il-input--invalid');
  });

  it('should call onBlur', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILInput onBlur={spy} />);

    Simulate.blur(input());

    expect(spy).toHaveBeenCalled();
  });
});
