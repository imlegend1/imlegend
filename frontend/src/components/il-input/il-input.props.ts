import { ILInputTypes } from './il-input.types';
import { Control } from '../../../interfaces/forms/control';

export interface ILInputProps extends Control<string>
{
  type?: ILInputTypes;
}
