import { ILFieldProps } from '../il-field/il-field.props';
import { ILInputProps } from './il-input.props';

export interface ILInputFieldProps extends ILFieldProps<string>, ILInputProps
{}
