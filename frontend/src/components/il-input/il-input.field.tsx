import { ILInput } from './il-input';
import { ILFieldHOC } from '../il-field/il-field.hoc';
import { ILInputFieldProps } from './il-input.field.props';

export const ILInputField = ILFieldHOC<ILInputFieldProps, string>(ILInput);
