import React, { ChangeEvent } from 'react';
import { ILInputProps } from './il-input.props';

export const ILInput = (props: ILInputProps): React.ReactElement =>
{
  const {
    value,
    disabled,
    type,
    invalid,
    onChange = (): void => (undefined as void),
    onBlur,
  }: ILInputProps = props;

  let classes: string = 'il-input';

  if (invalid)
  {
    classes += ' il-input--invalid';
  }

  return (
    <input
      value={value}
      readOnly={disabled}
      type={type}
      onChange={(event: ChangeEvent<HTMLInputElement>): unknown => onChange(event.target.value)}
      onBlur={onBlur}
      className={classes}
    />
  );
};
