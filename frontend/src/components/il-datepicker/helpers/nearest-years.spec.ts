import { nearestYears } from './nearest-years';

describe('nearestYears', () =>
{
  it('shoud return array of 41 numbers', () =>
  {
    expect(nearestYears(2000).length).toEqual(41);
  });

  it('should render 20 years earlier and later of current year value as year selector if value exists', () =>
  {
    expect(nearestYears(2020)[0]).toEqual(2000);
    expect(nearestYears(2020)[20]).toEqual(2020);
    expect(nearestYears(2020)[40]).toEqual(2040);
  });
});
