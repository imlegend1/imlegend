const SUNDAY: number = 7;

/**
 * @description Возвращает индекс дня(от 1 до 7)
 * @param date Входной день
 */
export const weekDay = (date: Date): number =>
{
  return date.getDay() || SUNDAY;
};
