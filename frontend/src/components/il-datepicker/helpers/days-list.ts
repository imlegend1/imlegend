/**
 * @description Формирует список дней в месяце на основе даты
 */
import { daysInMonth } from './days-in-month';
import { ILDatepickerDay } from '../il-datepicker.datepicker-day';
import { weekDay } from './week-day';

/**
 * @description Выдает список дней в году с подробной информацией по ним на основании даты
 * @param date Целевая дата
 */
export const daysList = (date: Date): ILDatepickerDay[] =>
{
  return Array(daysInMonth(date))
    .fill(null)
    .map((_: null, i: number): Date => new Date(date.getFullYear(), date.getMonth(), i + 1))
    .map((d: Date): ILDatepickerDay => (
      {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        atMonth: d.getDate(),
        atWeek: weekDay(d),
      }
    ));
};
