import { monthTable } from './month-table';
import { Benchmark } from '../../../../test-utils/benchmark';
import { repeat } from '../../../../test-utils/repeat';
import { Benchmarks } from '../../../../test-utils/benchmark.constants';

describe('monthTable', () =>
{
  it('should return two-dimensional array', () =>
  {
    expect(monthTable(new Date()) instanceof Array).toBeTruthy();
    expect(monthTable(new Date())[0] instanceof Array).toBeTruthy();
  });

  it('should return result contained every week in month', () =>
  {
    expect(monthTable(new Date(2021, 0, 10)).length).toEqual(5);
    expect(monthTable(new Date(2020, 10, 1)).length).toEqual(6);
  });

  it('should return result, whisch equal perfect snapshot', () =>
  {
    expect(monthTable(new Date(2021, 0, 10))).toMatchSnapshot();
    expect(monthTable(new Date(2020, 10, 1))).toMatchSnapshot();
  });

  it('should has hyperfast performance', () =>
  {
    const benchmark: Benchmark = new Benchmark();

    benchmark.start();
    repeat((iteration: number) =>
    {
      monthTable(new Date(iteration * 10000));
    });
    benchmark.end();

    expect(benchmark.time()).toBeLessThanOrEqual(Benchmarks.HyperFast);
  });
});
