/**
 * @description Возвращает количество дней в месяце
 * @param date Дата, из которой берется целевой месяц
 */
export const daysInMonth = (date: Date): number =>
{
  return new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
};
