import { daysList } from './days-list';

describe('daysList', () =>
{
  it('should return array with same length as days in month', () =>
  {
    expect(daysList(new Date(2021, 0)).length).toEqual(31);
    expect(daysList(new Date(2021, 1)).length).toEqual(28);
    expect(daysList(new Date(2020, 1)).length).toEqual(29);
    expect(daysList(new Date(2021, 2)).length).toEqual(31);
    expect(daysList(new Date(2021, 3)).length).toEqual(30);
    expect(daysList(new Date(2021, 4)).length).toEqual(31);
    expect(daysList(new Date(2021, 5)).length).toEqual(30);
    expect(daysList(new Date(2021, 6)).length).toEqual(31);
    expect(daysList(new Date(2021, 7)).length).toEqual(31);
    expect(daysList(new Date(2021, 8)).length).toEqual(30);
    expect(daysList(new Date(2021, 9)).length).toEqual(31);
    expect(daysList(new Date(2021, 10)).length).toEqual(30);
    expect(daysList(new Date(2021, 11)).length).toEqual(31);
  });

  it('should contain info about day in array items', () =>
  {
    expect(daysList(new Date(2021, 0))[0].year).toEqual(2021);
    expect(daysList(new Date(2021, 0))[0].month).toEqual(1);
    expect(daysList(new Date(2021, 0))[0].atMonth).toEqual(1);
    expect(daysList(new Date(2021, 0))[0].atWeek).toEqual(5);
  });
});
