import { daysInMonth } from './days-in-month';

describe('daysInMonth', () =>
{
  it('should return number', () =>
  {
    expect(typeof daysInMonth(new Date())).toEqual('number');
  });

  it('should return 31 days in January', () =>
  {
    expect(daysInMonth(new Date(2021, 0))).toEqual(31);
  });

  it('should return 28 days in February at non-leap year', () =>
  {
    expect(daysInMonth(new Date(2021, 1))).toEqual(28);
  });

  it('should return 29 days in February at non-leap year', () =>
  {
    expect(daysInMonth(new Date(2020, 1))).toEqual(29);
  });

  it('should return 31 days in March', () =>
  {
    expect(daysInMonth(new Date(2021, 2))).toEqual(31);
  });

  it('should return 30 days in April', () =>
  {
    expect(daysInMonth(new Date(2021, 3))).toEqual(30);
  });

  it('should return 31 days in May', () =>
  {
    expect(daysInMonth(new Date(2021, 4))).toEqual(31);
  });

  it('should return 30 days in June', () =>
  {
    expect(daysInMonth(new Date(2021, 5))).toEqual(30);
  });

  it('should return 30 days in July', () =>
  {
    expect(daysInMonth(new Date(2021, 6))).toEqual(31);
  });

  it('should return 31 days in August', () =>
  {
    expect(daysInMonth(new Date(2021, 7))).toEqual(31);
  });

  it('should return 30 days in September', () =>
  {
    expect(daysInMonth(new Date(2021, 8))).toEqual(30);
  });

  it('should return 31 days in October', () =>
  {
    expect(daysInMonth(new Date(2021, 9))).toEqual(31);
  });

  it('should return 30 days in November', () =>
  {
    expect(daysInMonth(new Date(2021, 10))).toEqual(30);
  });

  it('should return 31 days in December', () =>
  {
    expect(daysInMonth(new Date(2021, 11))).toEqual(31);
  });
});
