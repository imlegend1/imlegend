import { weekDay } from './week-day';

describe('weekDay', () =>
{
  it('should return week day of current day from 1 to 7', () =>
  {
    expect(weekDay(new Date(2021, 0, 4))).toEqual(1);
    expect(weekDay(new Date(2021, 0, 5))).toEqual(2);
    expect(weekDay(new Date(2021, 0, 6))).toEqual(3);
    expect(weekDay(new Date(2021, 0, 7))).toEqual(4);
    expect(weekDay(new Date(2021, 0, 8))).toEqual(5);
    expect(weekDay(new Date(2021, 0, 9))).toEqual(6);
    expect(weekDay(new Date(2021, 0, 10))).toEqual(7);
  });
});
