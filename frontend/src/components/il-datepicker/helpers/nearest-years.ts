/**
 * @description Количество годов для выбора ДО, или ПОСЛЕ текущего
 */
const YEARS_RANGE: number = 20;
/**
 * @description Количество годов рассчитывается, как текщий, 20 лет ранее, и 20 лет позднее
 */
const YEARS_COUNT: number = 2 * YEARS_RANGE + 1;

/**
 * @description Возвращает ближайшие года к указанному
 * @param year Номер года, к которому нужно подобрать список ближайших годов
 */
export const nearestYears = (year: number): number[] =>
{
  return Array(YEARS_COUNT)
    .fill(null)
    .map((_: null, i: number): number =>
    {
      const yearIndex: number = (year - YEARS_RANGE + i);

      return yearIndex;
    });
};
