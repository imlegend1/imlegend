import { flatDate } from './flat-date';

describe('flatDate', (): void =>
{
  it('should return date as string', (): void =>
  {
    expect(flatDate(new Date(2020, 10, 10))).toEqual('10.11.2020');
    expect(flatDate(new Date(2020, 0, 1))).toEqual('01.01.2020');
  });
});
