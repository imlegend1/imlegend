import { ILDatepickerDay } from '../il-datepicker.datepicker-day';
import { daysList } from './days-list';

const FIRST_DAY_AT_WEEK: number = 1;

/**
 * @default Возвращает двумерный массив для удобного рендера в таблице календаря
 * @param date дата, на основе которой показываются данные в календаре
 */
export const monthTable = (date: Date): ILDatepickerDay[][] =>
{
  const days: ILDatepickerDay[] = daysList(date);
  const weeks: ILDatepickerDay[][] = [];

  let weeksCount: number = 0;

  const pushDayToLastWeek = (day: ILDatepickerDay): void =>
  {
    weeks[weeksCount - 1].push(day);
  };
  const pushDayToNewWeek = (day: ILDatepickerDay): void =>
  {
    weeks.push([]);
    weeksCount += 1;
    weeks[weeksCount - 1].push(day);
  };

  pushDayToNewWeek(days[0]);

  for (let i = 1; i < days.length; i += 1)
  {
    if (days[i].atWeek === FIRST_DAY_AT_WEEK)
    {
      pushDayToNewWeek(days[i]);
    }
    else
    {
      pushDayToLastWeek(days[i]);
    }
  }

  return weeks;
};
