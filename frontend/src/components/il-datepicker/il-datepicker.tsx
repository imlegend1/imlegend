import React, { useState } from 'react';
import { ILDatepickerProps } from './il-datepicker.props';
import { ILDatepickerDropdown } from './il-datepicker-dropdown/il-datepicker-dropdown';
import { ILDatepickerControl } from './il-datepicker-control/il-datepicker-control';
import './il-datepicker.sass';

export const ILDatepicker = (props: ILDatepickerProps): React.ReactElement =>
{
  const { value, invalid, onChange, onBlur }: ILDatepickerProps = props;

  const [isShowDropdown, setShowDropdown] = useState<boolean>(false);

  return (
    <div className="il-datepicker">
      <ILDatepickerControl
        value={value}
        invalid={invalid}
        onClick={(): void => setShowDropdown(!isShowDropdown)}
        onBlur={onBlur}
      />
      {
        isShowDropdown
        &&
        <ILDatepickerDropdown
          date={value}
          onSelect={onChange}
        />
      }
    </div>
  );
};
