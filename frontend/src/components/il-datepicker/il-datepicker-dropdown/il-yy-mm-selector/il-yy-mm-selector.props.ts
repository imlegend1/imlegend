export interface ILYYMMSelectorProps
{
	onSelect?: (newDate: Date) => void;
	date?: Date;
}
