import React, { RefObject, useEffect, useRef } from 'react';
import { ILYYMMOptionsListProps } from './il-yy-mm-options-list.props';
import './il-yy-mm-options-list.sass';

/**
 * @description Величина(в пикселях) одной опции в списке
 */
const OPTION_HEIGHT: number = 90;
/**
 * @description Сколько опций может уместиться в барабане в один момент времени
 */
const OPTIONS_IN_VIEW: number = 5;

export const ILYYMMOptionsList = (props: ILYYMMOptionsListProps): React.ReactElement =>
{
  const { value, options, onSelect = (): void => undefined }: ILYYMMOptionsListProps = props;

  const containerRef: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null);
  const selectedRef: RefObject<HTMLDivElement> = useRef<HTMLDivElement>(null);

  useEffect((): void =>
  {
    const toSelectedInViewCenter: number = (selectedRef.current!.scrollTop - OPTIONS_IN_VIEW * OPTION_HEIGHT / 2);

    containerRef.current!.scrollTo({
      top: toSelectedInViewCenter,
    });
  }, [value]);

  return (
    <div
      ref={containerRef}
      className="il-yy-mm-options-list"
    >
      {
        options
          .map(([num, label]: [number, string]): React.ReactElement =>
          {
            let classes: string = 'il-yy-mm-options-list__option';

            const isOptionSelected: boolean = (value === num);

            if (isOptionSelected)
            {
              classes += ' il-yy-mm-options-list__option--selected';
            }

            return (
              <div
                key={num}
                ref={isOptionSelected ? selectedRef : null}
                onClick={(): void => onSelect(num)}
                className={classes}
              >
                {label}
              </div>
            );
          })
      }
    </div>
  );
};
