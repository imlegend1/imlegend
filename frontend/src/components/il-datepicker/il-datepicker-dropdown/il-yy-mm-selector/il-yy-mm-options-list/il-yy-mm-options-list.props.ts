export interface ILYYMMOptionsListProps
{
	value: number;
	options: [number, string][];
	onSelect?: (value: number) => void;
}
