import React from 'react';
import { monthLabels } from '../../il-datepicker.month';
import { ILYYMMSelectorProps } from './il-yy-mm-selector.props';
import { ILYYMMOptionsList } from './il-yy-mm-options-list/il-yy-mm-options-list';
import { nearestYears } from '../../helpers/nearest-years';
import './il-yy-mm-selector.sass';

type Option = [number, string];
type Options = Option[];

const FIRST_DAY_AT_MONTH_INDEX: number = 1;

export const ILYYMMSelector = (props: ILYYMMSelectorProps): React.ReactElement =>
{
  const { date = new Date(), onSelect = (): void => undefined }: ILYYMMSelectorProps = props;

  const monthesEntries: Options = Array.from(monthLabels.entries());
  const yearsEntries: Options = nearestYears(date.getFullYear()).map((year: number): Option => [year, String(year)]);

  const onMonthSelect = (month: number): void =>
  {
    onSelect(new Date(date.getFullYear(), month, FIRST_DAY_AT_MONTH_INDEX));
  };
  const onYearSelect = (year: number): void =>
  {
    onSelect(new Date(year, date.getMonth(), FIRST_DAY_AT_MONTH_INDEX));
  };

  return (
    <div className="il-yy-mm-selector">
      <ILYYMMOptionsList
        value={date!.getMonth()}
        options={monthesEntries}
        onSelect={onMonthSelect}
      />
      <ILYYMMOptionsList
        value={date!.getFullYear()}
        options={yearsEntries}
        onSelect={onYearSelect}
      />
    </div>
  );
};
