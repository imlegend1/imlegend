export interface ILDatepickerDropdownProps
{
	onSelect?: (newDate: Date) => void;
	date?: Date;
}
