import React, { useState } from 'react';
import { ILDatepickerDropdownProps } from './il-datepicker-dropdown.props';
import { ILDropdownSwitcher } from './il-dropdown-switcher/il-dropdown-switcher';
import { ILMonthTable } from './il-month-table/il-month-table';
import { ILYYMMSelector } from './il-yy-mm-selector/il-yy-mm-selector';
import './il-datepicker-dropdown.sass';

export const ILDatepickerDropdown = (props: ILDatepickerDropdownProps): React.ReactElement =>
{
  const { date = new Date(), onSelect }: ILDatepickerDropdownProps = props;

  const [ daysShowed, setDaysShowed ] = useState<boolean>(true);

  return (
    <div className="il-datepicker-dropdown">
      <ILDropdownSwitcher
        date={date}
        onClick={(): void => setDaysShowed(!daysShowed)}
      />
      {
      	daysShowed
          ?
          <ILMonthTable
            date={date}
            onSelect={onSelect}
          />
          :
          <ILYYMMSelector
            date={date}
            onSelect={onSelect}
          />
      }
    </div>
  );
};
