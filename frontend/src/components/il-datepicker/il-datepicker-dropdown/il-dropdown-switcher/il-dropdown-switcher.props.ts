export interface ILDropdownSwitcherProps
{
	date: Date;
	onClick?: () => void;
}
