import React from 'react';
import { monthShortenLabels } from '../../il-datepicker.month';
import { ILDropdownSwitcherProps } from './il-dropdown-switcher.props';
import './il-dropdown-switcher.sass';

export const ILDropdownSwitcher = (props: ILDropdownSwitcherProps): React.ReactElement =>
{
  const { date, onClick }: ILDropdownSwitcherProps = props;

  return (
    <div
      onClick={onClick}
      className="il-dropdown-switcher"
    >
      {monthShortenLabels.get(date.getMonth())}
    </div>
  );
};
