import React from 'react';
import { ILWeekDayProps } from './il-week-day.props';

export const ILWeekDay = (props: ILWeekDayProps): React.ReactElement =>
{
  const { selected, day, onSelect = (): void => undefined }: ILWeekDayProps = props;

  let classes: string = 'il-week-day';

  if (!day)
  {
    classes += ' il-week-day--hidden';
  }
  else if (selected)
  {
    classes += ' il-week-day--selected';
  }

  return (
    <div
      className={classes}
      onClick={(): void => onSelect(new Date(day!.year, day!.month - 1, day!.atMonth))}
    >
      {day?.atMonth}
    </div>
  );
};
