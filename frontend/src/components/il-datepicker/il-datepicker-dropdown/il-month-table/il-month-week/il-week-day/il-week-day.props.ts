import React from 'react';
import { ILDatepickerDay } from '../../../../il-datepicker.datepicker-day';
import { Maybe } from '../../../../../../../interfaces/structs/maybe';

export interface ILWeekDayProps
{
	selected: boolean;
	day: Maybe<ILDatepickerDay>;
	children?: string | React.ReactElement;
	onClick?: (day: number) => void | Promise<void>;
	onSelect?: (newDate: Date) => void;
}
