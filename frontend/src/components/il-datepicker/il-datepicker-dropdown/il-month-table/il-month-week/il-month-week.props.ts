import { ILDatepickerDay } from '../../../il-datepicker.datepicker-day';

export interface ILMonthWeekProps
{
	selectedDay: number;
	week: ILDatepickerDay[];
	index: number;
	onSelect?: (newDate: Date) => void;
}
