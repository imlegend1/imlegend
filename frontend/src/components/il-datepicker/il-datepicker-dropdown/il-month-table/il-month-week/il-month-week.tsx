import React from 'react';
import { ILMonthWeekProps } from './il-month-week.props';
import { Maybe } from '../../../../../../interfaces/structs/maybe';
import { ILDatepickerDay } from '../../../il-datepicker.datepicker-day';
import { ILWeekDay } from './il-week-day/il-week-day';
import './il-month-week.sass';

const WEEK_DAYS_COUNT: number = 7;

export const ILMonthWeek = (props: ILMonthWeekProps): React.ReactElement =>
{
  const { selectedDay, week, index, onSelect = (): void => undefined }: ILMonthWeekProps = props;

  return (
    <div className="il-month-week">
      {
        week.map((day: Maybe<ILDatepickerDay>, dayIndex: number): React.ReactElement =>
        {
          return (
            <ILWeekDay
              selected={selectedDay === day?.atMonth}
              key={day?.atWeek || (dayIndex + (index * WEEK_DAYS_COUNT))}
              day={day}
              onSelect={onSelect}
            />
          );
        })
      }
    </div>
  );
};
