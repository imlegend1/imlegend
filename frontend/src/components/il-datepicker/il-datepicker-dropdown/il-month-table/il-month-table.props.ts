export interface ILMonthTableProps
{
	date: Date;
	onSelect?: (newDate: Date) => void;
}
