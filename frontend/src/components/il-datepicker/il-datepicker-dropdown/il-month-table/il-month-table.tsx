import React from 'react';
import { monthTable } from '../../helpers/month-table';
import { ILDatepickerDay } from '../../il-datepicker.datepicker-day';
import { ILMonthTableProps } from './il-month-table.props';
import { ILMonthWeek } from './il-month-week/il-month-week';
import './il-month-table.sass';

const WEEK_DAYS_COUNT: number = 7;

export const ILMonthTable = (props: ILMonthTableProps): React.ReactElement =>
{
  const { date, onSelect = (): void => undefined }: ILMonthTableProps = props;

  return (
    <div className="il-month-table">
      {
        monthTable(date)
          .map((week: ILDatepickerDay[], weekIndex: number, arr: ILDatepickerDay[][]): React.ReactElement =>
          {
            let filledWeek: ILDatepickerDay[];
            
            const remaining: number = (WEEK_DAYS_COUNT - week.length);
            const inaccessibles: ILDatepickerDay[] = Array(remaining).fill(null);

            switch (weekIndex)
            {
            case 0:
              filledWeek = inaccessibles.concat(week);
              break;
            case arr.length - 1:
              filledWeek = week.concat(inaccessibles);
              break;
            default:
              filledWeek = week;
              break;
            }

            return (
              <ILMonthWeek
                key={week[0].atMonth}
                selectedDay={date.getDate()}
                week={filledWeek}
                index={weekIndex}
                onSelect={onSelect}
              />
            );
          })
      }
    </div>
  );
};
