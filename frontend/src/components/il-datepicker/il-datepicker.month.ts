export enum ILDatepickerMonth
{
	January,
	February,
	March,
	April,
	May,
	June,
	July,
	August,
	September,
	October,
	November,
	December,
}

export const monthLabels: Map<ILDatepickerMonth, string> = new Map<ILDatepickerMonth, string>(
  [
    [ILDatepickerMonth.January, 'Январь'],
    [ILDatepickerMonth.February, 'Февраль'],
    [ILDatepickerMonth.March, 'Март'],
    [ILDatepickerMonth.April, 'Апрель'],
    [ILDatepickerMonth.May, 'Май'],
    [ILDatepickerMonth.June, 'Июнь'],
    [ILDatepickerMonth.July, 'Июль'],
    [ILDatepickerMonth.August, 'Август'],
    [ILDatepickerMonth.September, 'Сентябрь'],
    [ILDatepickerMonth.October, 'Октябрь'],
    [ILDatepickerMonth.November, 'Ноябрь'],
    [ILDatepickerMonth.December, 'Декабрь'],
  ],
);

export const monthShortenLabels: Map<ILDatepickerMonth, string> = new Map<ILDatepickerMonth, string>(
  [
    [ILDatepickerMonth.January, 'Янв'],
    [ILDatepickerMonth.February, 'Фев'],
    [ILDatepickerMonth.March, 'Мар'],
    [ILDatepickerMonth.April, 'Апр'],
    [ILDatepickerMonth.May, 'Май'],
    [ILDatepickerMonth.June, 'Июн'],
    [ILDatepickerMonth.July, 'Июл'],
    [ILDatepickerMonth.August, 'Авг'],
    [ILDatepickerMonth.September, 'Сен'],
    [ILDatepickerMonth.October, 'Окт'],
    [ILDatepickerMonth.November, 'Ноя'],
    [ILDatepickerMonth.December, 'Дек'],
  ],
);
