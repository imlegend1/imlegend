import React, { useState } from 'react';
import { Simulate } from 'react-dom/test-utils';
import { ILDatepicker } from './il-datepicker';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';

describe('ILDatepicker', () =>
{
  let sandboxRenderer: SandboxRenderer;

  const datepickerInput = (): HTMLInputElement => sandboxRenderer.container.querySelector('input.il-datepicker__control') as HTMLInputElement;
  const datepickerDropdown = (): HTMLDivElement => sandboxRenderer.container.querySelector('div.il-datepicker-dropdown') as HTMLDivElement;
  const datepickerDropdownSwitcher = (): HTMLDivElement => sandboxRenderer.container.querySelector('div.il-dropdown-switcher') as HTMLDivElement;
  const monthTable = (): HTMLDivElement => sandboxRenderer.container.querySelector('div.il-month-table') as HTMLDivElement;
  const monthWeeks = (): NodeListOf<HTMLDivElement> => monthTable().querySelectorAll('div.il-month-week') as NodeListOf<HTMLDivElement>;
  const monthDays = (): NodeListOf<HTMLDivElement> => monthTable().querySelectorAll('div.il-month-week>div.il-week-day') as NodeListOf<HTMLDivElement>;
  const YYMMSelector = (): HTMLDivElement => sandboxRenderer.container.querySelector('div.il-yy-mm-selector') as HTMLDivElement;
  const monthSelector = (): HTMLDivElement => sandboxRenderer.container.querySelectorAll('div.il-yy-mm-selector>div.il-yy-mm-options-list')[0] as HTMLDivElement;
  const monthSelectorOptions = (): NodeListOf<HTMLDivElement> => monthSelector().childNodes as NodeListOf<HTMLDivElement>;
  const yearSelector = (): HTMLDivElement => sandboxRenderer.container.querySelectorAll('div.il-yy-mm-selector>div.il-yy-mm-options-list')[1] as HTMLDivElement;
  const yearSelectorOptions = (): NodeListOf<HTMLDivElement> => yearSelector().childNodes as NodeListOf<HTMLDivElement>;

  beforeAll((): void =>
  {
    Element.prototype.scrollTo = (): void => undefined;
  });

  beforeEach((): void =>
  {
    jest.useFakeTimers();
  });

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;

    jest.useRealTimers();
    jest.resetAllMocks();
  });

  it('should render container with corresponding class', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker/>);

    expect(sandboxRenderer.container.querySelector('div.il-datepicker')).not.toBeNull();
  });

  it('should render input field control with corresponding class', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker/>);

    expect(datepickerInput()).not.toBeNull();
  });

  it('should render icon container with corresponding class', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker/>);

    expect(sandboxRenderer.container.querySelector('div.il-datepicker__icon')).not.toBeNull();
  });

  it('should show datepicker dropdown div with corresponding class after click on input or icon', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdown()).not.toBeNull();
  });

  it('should has switcher with corresponding class inside dropdown', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher()).not.toBeNull();
  });

  it('should has shorten month label from props value inside switcher', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 0, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Янв');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 1, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Фев');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 2, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Мар');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 3, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Апр');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 4, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Май');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 5, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Июн');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 6, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Июл');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 7, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Авг');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 8, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Сен');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 9, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Окт');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 10, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Ноя');
    sandboxRenderer?.demolite();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 11, 2)}/>);
    Simulate.click(datepickerInput());
    expect(datepickerDropdownSwitcher().innerHTML).toEqual('Дек');
    sandboxRenderer?.demolite();
  });

  it('should has month table on dropdown', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker/>);
    Simulate.click(datepickerInput());
    expect(monthTable()).not.toBeNull();
  });

  it('should render div for every week even eksik', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2021, 0)}/>);
    Simulate.click(datepickerInput());
    expect(monthWeeks().length).toEqual(5);
  });

  it('should render div for everyday in week for current day and missing weekdays in neighboring monthes', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)}/>);
    Simulate.click(datepickerInput());
    expect(monthDays().length).toEqual(42);
  });

  it('should render day index in div day in current month', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)}/>);
    Simulate.click(datepickerInput());

    expect(monthDays()[6].innerHTML).toEqual('1');
    expect(monthDays()[7].innerHTML).toEqual('2');
    expect(monthDays()[8].innerHTML).toEqual('3');
    expect(monthDays()[9].innerHTML).toEqual('4');
    expect(monthDays()[35].innerHTML).toEqual('30');
  });

  it('should render day as hidden if not in current month', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)}/>);
    Simulate.click(datepickerInput());

    expect(monthDays()[0].className).toMatch('il-week-day--hidden');
    expect(monthDays()[1].className).toMatch('il-week-day--hidden');
    expect(monthDays()[2].className).toMatch('il-week-day--hidden');
    expect(monthDays()[3].className).toMatch('il-week-day--hidden');
    expect(monthDays()[4].className).toMatch('il-week-day--hidden');
    expect(monthDays()[5].className).toMatch('il-week-day--hidden');
  });

  it('should render selected day with same class', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10, 3)}/>);
    Simulate.click(datepickerInput());

    expect(monthDays()[8].className).toMatch('il-week-day--selected');
  });

  it('should display month and year selector after click on switcher instead month table', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)}/>);
    Simulate.click(datepickerInput());

    expect(YYMMSelector()).toBeNull();

    Simulate.click(datepickerDropdownSwitcher());

    expect(YYMMSelector()).not.toBeNull();
    expect(monthTable()).toBeNull();
  });

  it('should display month table after double click on switcher', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)}/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());
    Simulate.click(datepickerDropdownSwitcher());

    expect(YYMMSelector()).toBeNull();
    expect(monthTable()).not.toBeNull();
  });

  it('should render each month with full labels as month selector', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)}/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());

    expect(monthSelector()).not.toBeNull();
    expect(monthSelector().children.length).toEqual(12);
    expect(monthSelector().children[0].className).toEqual('il-yy-mm-options-list__option');

    expect(monthSelector().children[0].innerHTML).toEqual('Январь');
    expect(monthSelector().children[1].innerHTML).toEqual('Февраль');
    expect(monthSelector().children[2].innerHTML).toEqual('Март');
    expect(monthSelector().children[3].innerHTML).toEqual('Апрель');
    expect(monthSelector().children[4].innerHTML).toEqual('Май');
    expect(monthSelector().children[5].innerHTML).toEqual('Июнь');
    expect(monthSelector().children[6].innerHTML).toEqual('Июль');
    expect(monthSelector().children[7].innerHTML).toEqual('Август');
    expect(monthSelector().children[8].innerHTML).toEqual('Сентябрь');
    expect(monthSelector().children[9].innerHTML).toEqual('Октябрь');
    expect(monthSelector().children[10].innerHTML).toEqual('Ноябрь');
    expect(monthSelector().children[11].innerHTML).toEqual('Декабрь');
  });

  it('should render 20 years earlier and later of current year value as year selector if value exists', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)}/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());

    expect(yearSelector()).not.toBeNull();
    expect(yearSelector().children.length).toEqual(41);
    expect(yearSelector().children[0].className).toEqual('il-yy-mm-options-list__option');

    expect(yearSelector().children[0].innerHTML).toEqual('2000');
    expect(yearSelector().children[20].innerHTML).toEqual('2020');
    expect(yearSelector().children[40].innerHTML).toEqual('2040');
  });

  it('should render selected month and year with same classes', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)}/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());

    expect(monthSelector().children[10].className).toEqual('il-yy-mm-options-list__option il-yy-mm-options-list__option--selected');
    expect(yearSelector().children[20].className).toEqual('il-yy-mm-options-list__option il-yy-mm-options-list__option--selected');
  });

  it('should render 20 years earlier and later of current year date if value not exists', () =>
  {
    const currentDate: Date = new Date();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());

    expect(yearSelector()).not.toBeNull();
    expect(yearSelector().children.length).toEqual(41);

    expect(yearSelector().children[0].innerHTML).toEqual(String(currentDate.getFullYear() - 20));
    expect(yearSelector().children[40].innerHTML).toEqual(String(currentDate.getFullYear() + 20));
  });

  it('should call onChange callback after click on month option', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker onChange={spy}/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());
    Simulate.click(monthSelectorOptions()[0]);

    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should call onChange callback after click on year option', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker onChange={spy}/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());
    Simulate.click(yearSelectorOptions()[0]);

    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should scroll after show or change selected month option', () =>
  {
    const TestComponent = (): React.ReactElement =>
    {
      const [date, setDate] = useState<Date>(new Date(2020, 10));

      return (<ILDatepicker value={date} onChange={setDate}/>);
    };

    sandboxRenderer = new SandboxRenderer(<TestComponent/>);

    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());

    const spy = jest.spyOn(monthSelector(), 'scrollTo');

    jest.advanceTimersByTime(0);
    expect(spy).toHaveBeenCalledTimes(1);

    Simulate.click(monthSelectorOptions()[0]);
    jest.advanceTimersByTime(0);

    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should scroll after show or change selected year option', () =>
  {
    const TestComponent = (): React.ReactElement =>
    {
      const [date, setDate] = useState<Date>(new Date(2020, 10));

      return (<ILDatepicker value={date} onChange={setDate}/>);
    };

    sandboxRenderer = new SandboxRenderer(<TestComponent/>);

    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());

    const spy = jest.spyOn(yearSelector(), 'scrollTo');

    jest.advanceTimersByTime(0);
    expect(spy).toHaveBeenCalledTimes(1);

    Simulate.click(yearSelectorOptions()[0]);
    jest.advanceTimersByTime(0);

    expect(spy).toHaveBeenCalledTimes(2);
  });

  it('should call onChange with first month of day after click on month option', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)} onChange={spy}/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());
    Simulate.click(monthSelectorOptions()[0]);

    expect(spy).toHaveBeenCalledWith(new Date(2020, 0));
  });

  it('should call onChange with earlier year than selected after click on year option', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10)} onChange={spy}/>);
    Simulate.click(datepickerInput());

    Simulate.click(datepickerDropdownSwitcher());
    Simulate.click(yearSelectorOptions()[0]);

    expect(spy).toHaveBeenCalledWith(new Date(2000, 10));
  });

  it('should call onChange with earlier day than selected after click on day option', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10, 10)} onChange={spy}/>);
    Simulate.click(datepickerInput());

    Simulate.click(monthDays()[6]);

    expect(spy).toHaveBeenCalledWith(new Date(2020, 10, 1));
  });

  it('should render date in input', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker value={new Date(2020, 10, 10)} />);

    expect(datepickerInput().value).toEqual('10.11.2020');
  });

  it('should add invalid class if component rendered as invalid', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILDatepicker invalid />);

    expect(datepickerInput().className).toMatch('il-datepicker__control--invalid');
  });

  it('should call onBlur', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILDatepicker onBlur={spy}/>);

    Simulate.blur(datepickerInput());

    expect(spy).toHaveBeenCalledTimes(1);
  });
});
