export interface ILDatepickerControlProps
{
	value?: Date;
	invalid?: boolean;
	onClick: () => void;
	onBlur?: () => void;
}
