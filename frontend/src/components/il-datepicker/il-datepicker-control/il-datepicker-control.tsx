import React from 'react';
import { ILDatepickerControlProps } from './il-datepicker-control.props';
import { flatDate } from '../helpers/flat-date';

export const ILDatepickerControl = (props: ILDatepickerControlProps): React.ReactElement =>
{
  const { value, invalid, onClick, onBlur }: ILDatepickerControlProps = props;

  let inputClasses: string = 'il-datepicker__control';

  if (invalid)
  {
    inputClasses += ' il-datepicker__control--invalid';
  }

  return (
    <>
      <input
        value={value ? flatDate(value) : ''}
        onClick={onClick}
        onBlur={onBlur}
        readOnly
        className={inputClasses}
        type="text"
      />
      <div
        className="il-datepicker__icon"
      />
    </>
  );
};
