import { Control } from '../../../interfaces/forms/control';

export interface ILDatepickerProps extends Control<Date>
{
	placeholer?: string;
}
