import { ILDatepickerFieldProps } from './il-datepicker.field.props';
import { ILFieldHOC } from '../il-field/il-field.hoc';
import { ILDatepicker } from './il-datepicker';

export const ILDatepickerField = ILFieldHOC<ILDatepickerFieldProps, Date>(ILDatepicker);
