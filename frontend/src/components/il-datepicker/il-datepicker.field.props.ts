import { ILFieldProps } from '../il-field/il-field.props';
import { ILDatepickerProps } from './il-datepicker.props';

export interface ILDatepickerFieldProps extends ILFieldProps<Date>, ILDatepickerProps
{}
