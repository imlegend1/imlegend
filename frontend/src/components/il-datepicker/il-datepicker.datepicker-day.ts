export interface ILDatepickerDay
{
	/**
	 * @description Год
	 */
	year: number;
	/**
	 * @description Месяц(от 1 до 12)
	 */
	month: number
	/**
	 * @description День в месяце(от 1 до 31)
	 */
	atMonth: number;
	/**
	 * @description День в неделе(от 1 до 7)
	 */
	atWeek: number;
}
