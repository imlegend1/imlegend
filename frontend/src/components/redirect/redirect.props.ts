export interface RedirectProps
{
	/**
	 * @description Url на который произведется перенаправление при рендеринге компонента
	 * @example { to: '/auth/sign-in' }
	 */
	to: string;
}
