import React, { useEffect } from 'react';
import { NextRouter, useRouter } from 'next/router';
import { RedirectProps } from './redirect.props';
import environmentService from '../../services/environment.service';

/**
 * @description Компонент для перенаправления на другие страницы вместо рендеринга HTML
 */
export const Redirect = (props: RedirectProps): React.ReactElement =>
{
  const { to: url } = props;

  const router: NextRouter = useRouter();

  useEffect((): void =>
  {
    if (environmentService.isBrowser())
    {
      router.push(url);
    }
  });

  return (<></>);
};
