import React from 'react';
import * as routerModule from 'next/router';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';
import { Redirect } from './redirect';

jest.mock('next/router');

describe('Redirect', (): void =>
{
  let sandboxRenderer: SandboxRenderer;

  class RouterClass
  {
    push(): void
    {}
  }

  beforeEach((): void =>
  {
    (routerModule.useRouter as unknown as jest.Mock).mockReturnValue(new RouterClass());
  });

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;

    jest.resetAllMocks();
  });

  it('should return empty fragment by default', (): void =>
  {
    sandboxRenderer = new SandboxRenderer(<Redirect to="/any"/>);

    expect(sandboxRenderer.container.innerHTML).toEqual('');
  });

  it('should redirect on new url after rendering', (): void =>
  {
    const url: string = '/auth';

    jest.spyOn(RouterClass.prototype, 'push');

    sandboxRenderer = new SandboxRenderer(<Redirect to={url}/>);

    expect(RouterClass.prototype.push).toHaveBeenCalledTimes(1);
    expect(RouterClass.prototype.push).toHaveBeenCalledWith('/auth');
  });
});
