export interface ILButtonProps
{
	disabled?: boolean;
	/**
	 * @description Контент внутри кнопки
	 */
	children?: React.ReactElement | string;
	/**
	 * @description Если существует, то возвращает не кнопку, а элемент ссылки с такой же версткой, но редиректом
	 */
	href?: string;
	onClick?: () => void | Promise<void>;
}
