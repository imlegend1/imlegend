import React from 'react';
import { Simulate } from 'react-dom/test-utils';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';
import { ILButton } from './il-button';
import Mock = jest.Mock;

describe('ILButtonHOC', () =>
{
  let sandboxRenderer: SandboxRenderer;

  const button = (): HTMLElement => sandboxRenderer.container.querySelector('button') as HTMLElement;

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;
  });

  it('should render button element', () =>
  {
	  sandboxRenderer = new SandboxRenderer(<ILButton />);

    expect(sandboxRenderer.container.querySelector('button')).not.toBeNull();
  });

  it('should render text content inside button', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILButton>Hello, World!</ILButton>);

    expect(button().innerHTML).toEqual('Hello, World!');
  });

  it('should call onClick callback after click on button', () =>
  {
  	const spy: Mock = jest.fn();

  	sandboxRenderer = new SandboxRenderer(<ILButton onClick={spy}/>);

	  Simulate.click(button());

	  expect(spy).toHaveBeenCalled();
  });

  it('should not call onClick if disabled', () =>
  {
	  const spy: Mock = jest.fn();

	  sandboxRenderer = new SandboxRenderer(<ILButton disabled onClick={spy}/>);

	  Simulate.click(button());

	  expect(spy).not.toHaveBeenCalled();
  });
});
