import React from 'react';
import { ILButtonProps } from './il-button.props';

export const ILButton = (props: ILButtonProps): React.ReactElement =>
{
  const { children, disabled, onClick } = props;

  return (
  	<button
		  disabled={disabled}
		  onClick={onClick}
		  type="button"
	  >
		  {children}
  	</button>
  );
};
