import React from 'react';
import * as routerModule from 'next/router';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';
import { Maybe } from '../../../interfaces/structs/maybe';
import { GuardedRoute } from './guarded-route';

jest.mock('next/router');

describe('GuardedRoute', (): void =>
{
  let sandboxRenderer: SandboxRenderer;

  class RouterClass
  {
    push(): void
    {}
  }

  const ABlock = (): React.ReactElement => (<div id="A">A</div>);

  const A = (): Maybe<HTMLDivElement> => sandboxRenderer.container.querySelector('#A') as HTMLDivElement;

  beforeEach((): void =>
  {
    (routerModule.useRouter as unknown as jest.Mock).mockReturnValue(new RouterClass());
  });

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;
  });

  it('it should render component if condition is true', (): void =>
  {
    sandboxRenderer = new SandboxRenderer(
      <GuardedRoute
        canRender={() => true}
        redirectOnFail="/sign-in"
      >
        <ABlock />
      </GuardedRoute>
    );

    expect(A()).not.toBeNull();
  });

  it('it should redirect to props url if condition is false', (): void =>
  {
    jest.spyOn(RouterClass.prototype, 'push');

    sandboxRenderer = new SandboxRenderer(
      <GuardedRoute
        canRender={() => false}
        redirectOnFail="/sign-in"
      >
        <ABlock />
      </GuardedRoute>
    );

    expect(RouterClass.prototype.push).toHaveBeenCalledTimes(1);
    expect(RouterClass.prototype.push).toHaveBeenCalledWith('/sign-in');
  });
});
