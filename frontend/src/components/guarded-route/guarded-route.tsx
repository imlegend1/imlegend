import React from 'react';
import { GuardedRouteProps } from './guarded-route.props';
import { Guard } from '../guard/guard';
import { Redirect } from '../redirect/redirect';

/**
 * @description Компонент для использования в папке pages - защищает приложение от несанкционированного рендеринга
 */
export const GuardedRoute = (props: GuardedRouteProps): React.ReactElement =>
{
  const { canRender, children, redirectOnFail: failUrl } = props;

  return (
    <Guard
      condition={canRender}
      success={children}
      fail={<Redirect to={failUrl} />}
    />
  );
};
