export interface GuardedRouteProps
{
  /**
   * @description Если функция возвращает true, то компонент рендерит children
   */
  canRender: () => boolean;
  /**
   * @description При неудачной проверке canRender перенаправляет пользователя на этот url
   */
  redirectOnFail: string;
  /**
   * @description React код, который нужно "защитить" от несанкционированного рендеринга
   */
  children: React.ReactElement;
}
