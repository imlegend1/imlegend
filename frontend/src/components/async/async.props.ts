import React from 'react';

export interface AsyncProps<T, E>
{
	/**
	 * @description Асинхронный запрос, результат которого должен отрендерить компонент Async
	 */
	future: Promise<T>;
	/**
	 * @description Функция, которая обрабатывает ответ асинхронного запроса и переводит его в JSX
	 * @param val Ответ асинхронного запроса
	 */
	children: (val: T) => React.ReactElement;
	/**
	 * @description Заглушка на время работы асинхронного запроса
	 */
	loading?: React.ReactElement;
	/**
	 * @description Заглушка при ошибке асинхронного запроса
	 */
	catched?: (error: E) => React.ReactElement;
}
