import React, { useEffect, useState } from 'react';
import { AsyncProps } from './async.props';
import { AsyncStatus } from './async.constants';
import { Maybe } from '../../../interfaces/structs/maybe';

export const Async = <T, E>(props: AsyncProps<T, E>): React.ReactElement =>
{
  const { future, children, loading, catched }: AsyncProps<T, E> = props;

  const [value, setValue] = useState<Maybe<T>>(null);
  const [error, setError] = useState<Maybe<E>>(null);
  const [status, setStatus] = useState<AsyncStatus>(AsyncStatus.Loading);

  const afterFutureResolved = async (): Promise<void> =>
  {
    try
    {
      setValue(await future);
      setStatus(AsyncStatus.Loaded);
    }
    catch (e)
    {
      setError(e);
      setStatus(AsyncStatus.Catched);
    }
  };

  useEffect(() =>
  {
    afterFutureResolved();
  });

  switch (status)
  {
  case AsyncStatus.Loaded:
    return (<>{children(value!)}</>);
  case AsyncStatus.Catched:
    return (<>{catched ? catched(error!) : loading}</>);
  default:
    return (<>{loading}</>);
  }
};
