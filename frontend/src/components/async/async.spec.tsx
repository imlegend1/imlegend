import React from 'react';
import { act } from 'react-dom/test-utils';
import { Async } from './async';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';

describe('Async', () =>
{
  let sandboxRenderer: SandboxRenderer;
  const futureFactory = (): Promise<number> =>
  {
    return new Promise<number>((resolve: (num: number) => void) =>
    {
      resolve(15);
    });
  };

  beforeEach(() =>
  {
    jest.useFakeTimers();
  });

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    jest.useRealTimers();
  });

  it('should render content after promise resolver', async () =>
  {
    const future: Promise<number> = futureFactory();
    sandboxRenderer = new SandboxRenderer(
      <Async
        future={future}
      >
        {(val: number): React.ReactElement => (<div id="FutureRendered">{val}</div>)}
      </Async>
    );

    await act(async () =>
    {
      await future;
    });

    expect(sandboxRenderer.container.querySelector('#FutureRendered')).not.toBeNull();
    expect(sandboxRenderer.container.querySelector('#FutureRendered')?.innerHTML).toBe('15');
  });

  it('should render loading mock while waiting for future', async () =>
  {
    const future: Promise<number> = futureFactory();
    sandboxRenderer = new SandboxRenderer(
      <Async
        future={future}
        loading={<div id="LoadingRendered">Loading...</div>}
      >
        {(val: number): React.ReactElement => (<div id="FutureRendered">{val}</div>)}
      </Async>
    );

    expect(sandboxRenderer.container.querySelector('#LoadingRendered')).not.toBeNull();

    await act(async () =>
    {
      await future;
    });
  });

  it('should render catched mock if loading failed for future', async () =>
  {
    const future: Promise<number> = futureFactory().then((): Promise<number> => Promise.reject('Catched!'));
    sandboxRenderer = new SandboxRenderer(
      <Async
        future={future}
        catched={(errorText: string): React.ReactElement => <div id="CatchedRendered">{errorText}</div>}
      >
        {(val: number): React.ReactElement => (<div id="FutureRendered">{val}</div>)}
      </Async>
    );

    await act(async () =>
    {
      await future.catch(() => true);
    });

    expect(sandboxRenderer.container.querySelector('#CatchedRendered')).not.toBeNull();
    expect(sandboxRenderer.container.querySelector('#CatchedRendered')?.innerHTML).toEqual('Catched!');
  });
});
