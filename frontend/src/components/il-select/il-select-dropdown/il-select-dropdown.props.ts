import { ILSelectOptionProps } from './il-select-option/il-select-option.props';

export interface ILSelectDropdownProps
{
	options?: ILSelectOptionProps[];
	noOptionsStub?: string;
	altActionLabel?: string | React.ReactElement;
	onChange?: (value: string) => void | Promise<void>;
	onEditOption?: (value: string) => void | Promise<void>;
	onDeleteOption?: (value: string) => void | Promise<void>;
	onAltAction?: () => void | Promise<void>;
}
