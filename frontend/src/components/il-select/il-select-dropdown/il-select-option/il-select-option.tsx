import React, { SyntheticEvent } from 'react';
import { ILSelectOptionProps } from './il-select-option.props';
import './il-select-option.sass';

export const ILSelectOption = (props: ILSelectOptionProps): React.ReactElement =>
{
  const { value, children, isSelected, onClick = (): null => null, onEdit, onDelete } = props;

  const callEdit = (e: SyntheticEvent): void =>
  {
    e.stopPropagation();
    onEdit!(value as string);
  };
  const callDelete = (e: SyntheticEvent): void =>
  {
    e.stopPropagation();
    onDelete!(value as string);
  };

  let classes: string = 'il-select-option';

  if (isSelected)
  {
    classes += ' il-select-option--selected';
  }

  return (
    <div
      className={classes}
      onMouseDown={(): void => onClick(value as string)}
    >
      {children}
      <div>
        {onEdit && <div onMouseDown={callEdit} className="il-select-option__edit-action" />}
        {onDelete && <div onMouseDown={callDelete} className="il-select-option__delete-action" />}
      </div>
    </div>
  );
};
