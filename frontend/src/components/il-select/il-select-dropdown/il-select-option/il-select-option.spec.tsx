import React from 'react';
import renderer from 'react-test-renderer';
import { Simulate } from 'react-dom/test-utils';
import { ILSelectOption } from './il-select-option';
import { SandboxRenderer } from '../../../../../test-utils/sandbox-renderer';
import { repeat } from '../../../../../test-utils/repeat';
import { Benchmark } from '../../../../../test-utils/benchmark';
import { Benchmarks } from '../../../../../test-utils/benchmark.constants';
import Mock = jest.Mock;

describe('ILSelectOption', () =>
{
  let sandboxRenderer: SandboxRenderer;

  const option = (): HTMLElement => sandboxRenderer.container.querySelector('div.il-select-option') as HTMLElement;

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;
  });

  it('should render div with option class', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelectOption/>);

    expect(option()).not.toBeNull();
  });

  it('should render input text inside div', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelectOption>ButtonText</ILSelectOption>);

    expect(option().innerHTML).toMatch('ButtonText');
  });

  it('should has selected class if rendered as selected', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelectOption isSelected/>);

    expect(option().className).toContain('il-select-option--selected');
  });

  it('should call onClick with value after click on root element', () =>
  {
    const spy: Mock = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILSelectOption value="123" onClick={spy}/>);

    Simulate.mouseDown(option());

    expect(spy).toHaveBeenCalledWith('123');
  });

  it('should render div with edit action and class if has same props', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelectOption onEdit={jest.fn()}>My Action</ILSelectOption>);

    expect(option().querySelector('div.il-select-option__edit-action')).not.toBeNull();
  });

  it('should call edit action with value after click on edit action div', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILSelectOption onEdit={spy} value="56">My Action</ILSelectOption>);

    Simulate.mouseDown(option().querySelector('div.il-select-option__edit-action') as HTMLDivElement);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('56');
  });

  it('should render div with delete action and class if has same props', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelectOption onDelete={jest.fn()}>My Action</ILSelectOption>);

    expect(option().querySelector('div.il-select-option__delete-action')).not.toBeNull();
  });

  it('should call delete action with value after click on delete action div', () =>
  {
    const spy = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILSelectOption onDelete={spy} value="56">My Action</ILSelectOption>);

    Simulate.mouseDown(option().querySelector('div.il-select-option__delete-action') as HTMLDivElement);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('56');
  });

  it('should be rendered with all props hyperfast', () =>
  {
    const benchmark: Benchmark = new Benchmark();

    benchmark.start();
    repeat((i: number): void =>
    {
      <ILSelectOption value={String(i)} onClick={(): void => (undefined as void)} isSelected={i%2 === 0} />;
    });
    benchmark.end();

    expect(benchmark.time()).toBeLessThanOrEqual(Benchmarks.HyperFast);
  });

  it('should equal previous snapshot', (): void =>
  {
    expect(
      renderer.create(
        <ILSelectOption
          value="someValue"
          onClick={(): void => (undefined as void)}
          isSelected
        />
      ).toJSON()
    ).toMatchSnapshot();

    expect(
      renderer.create(
        <ILSelectOption
          value="someValue"
          onClick={(): void => (undefined as void)}
          onEdit={(): void => (undefined as void)}
          onDelete={(): void => (undefined as void)}
          isSelected
        />
      ).toJSON()
    ).toMatchSnapshot();
  });
});
