export interface ILSelectOptionProps
{
	/**
	 * @description Значение опции
	 */
	value?: string;
	/**
	 * @description Описание опции
	 */
	children?: string;
	isSelected?: boolean;
	onClick?: (val: string) => void;
	onEdit?: (val: string) => void;
	onDelete?: (val: string) => void;
}
