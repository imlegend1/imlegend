import React from 'react';
import renderer from 'react-test-renderer';
import { SandboxRenderer } from '../../../../../test-utils/sandbox-renderer';
import { ILSelectEmpty } from './il-select-empty';
import { Benchmark } from '../../../../../test-utils/benchmark';
import { repeat } from '../../../../../test-utils/repeat';
import { Benchmarks } from '../../../../../test-utils/benchmark.constants';

describe('ILSelectEmpty', () =>
{
  let sandboxRenderer: SandboxRenderer;

  const message = (): HTMLElement => sandboxRenderer.container.querySelector('div.il-select-empty') as HTMLElement;

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;
  });

  it('should render div with class', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelectEmpty/>);

    expect(message()).not.toBeNull();
  });

  it('should contain message', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelectEmpty>Message</ILSelectEmpty>);

    expect(message().innerHTML).toEqual('Message');
  });

  it('should be rendered hyperfast', () =>
  {
    const benchmark: Benchmark = new Benchmark();

    benchmark.start();
    repeat((): void =>
    {
      <ILSelectEmpty>Rendered</ILSelectEmpty>;
    });
    benchmark.end();

    expect(benchmark.time()).toBeLessThanOrEqual(Benchmarks.HyperFast);
  });

  it('should be same as previous snapshot', () =>
  {
    expect(renderer.create(<ILSelectEmpty>Perfect render!</ILSelectEmpty>).toJSON()).toMatchSnapshot();
  });
});
