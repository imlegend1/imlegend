import React from 'react';
import { ILSelectEmptyProps } from './il-select-empty.props';

export const ILSelectEmpty = ({ children }: ILSelectEmptyProps): React.ReactElement =>
{
  return (<div className="il-select-empty">{children}</div>);
};
