import React from 'react';
import renderer from 'react-test-renderer';
import { Simulate } from 'react-dom/test-utils';
import { SandboxRenderer } from '../../../../../test-utils/sandbox-renderer';
import { ILOptionsList } from './il-options-list';
import { ILSelectOptionProps } from '../il-select-option/il-select-option.props';
import { Benchmark } from '../../../../../test-utils/benchmark';
import { repeat } from '../../../../../test-utils/repeat';
import { Benchmarks } from '../../../../../test-utils/benchmark.constants';
import Mock = jest.Mock;

describe('ILOptionsList', () =>
{
  let sandboxRenderer: SandboxRenderer;

  const list = (): NodeListOf<HTMLElement> => sandboxRenderer.container.querySelectorAll('div.il-select-option');
  const options = (count: number): ILSelectOptionProps[] =>
  {
    return Array(count)
      .fill(null)
      .map((_: null, i: number): ILSelectOptionProps => ({ value: `value ${i + 1}`, children: `label ${i + 1}` }));
  };

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;
  });

  it('should render nothing by default', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILOptionsList/>);

    expect(sandboxRenderer.container.innerHTML).toEqual('');
  });

  it('should render each option as component', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILOptionsList options={options(10)}/>);

    expect(list().length).toEqual(10);
  });

  it('should render each option label', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILOptionsList options={options(4)}/>);

    expect(list()[0].innerHTML).toMatch('label 1');
    expect(list()[1].innerHTML).toMatch('label 2');
    expect(list()[2].innerHTML).toMatch('label 3');
    expect(list()[3].innerHTML).toMatch('label 4');
  });

  it('should call click handler with option value after click at option', () =>
  {
    const spy: Mock = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILOptionsList options={options(4)} onSelect={spy}/>);

    Simulate.mouseDown(list()[1]);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('value 2');
  });

  it('should call edit handler with option value after call option edit action', () =>
  {
    const spy: Mock = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILOptionsList options={options(4)} onEdit={spy}/>);

    Simulate.mouseDown(list()[1].querySelector('.il-select-option__edit-action') as HTMLElement);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('value 2');
  });

  it('should call edit handler with option value after call option edit action', () =>
  {
    const spy: Mock = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILOptionsList options={options(4)} onDelete={spy}/>);

    Simulate.mouseDown(list()[1].querySelector('.il-select-option__delete-action') as HTMLElement);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('value 2');
  });

  it('should be rendered hyperfast', () =>
  {
    const benchmark: Benchmark = new Benchmark();

    benchmark.start();
    repeat((): void =>
    {
      <ILOptionsList options={options(25)} />;
    });
    benchmark.end();

    expect(benchmark.time()).toBeLessThanOrEqual(Benchmarks.HyperFast);
  });

  it('should be same as previous snapshot', () =>
  {
    expect(renderer.create(<ILOptionsList options={options(25)} />).toJSON()).toMatchSnapshot();
  });
});
