import React from 'react';
import { ILOptionsListProps } from './il-options-list.props';
import { ILSelectOption } from '../il-select-option/il-select-option';
import { ILSelectOptionProps } from '../il-select-option/il-select-option.props';

export const ILOptionsList = (props: ILOptionsListProps): React.ReactElement =>
{
  const { options = [], onSelect, onEdit, onDelete } = props;

  return (<>
	  {
	  	options.map(({ value, children, isSelected }: ILSelectOptionProps): React.ReactElement =>
		  {
			  return (
			  	<ILSelectOption
					  key={value}
					  value={value}
					  isSelected={isSelected}
					  onClick={onSelect}
					  onEdit={onEdit}
					  onDelete={onDelete}
				  >
					  {children}
			  	</ILSelectOption>
			  );
		  })
	  }
	  </>);
};
