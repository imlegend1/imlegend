import { ILSelectOptionProps } from '../il-select-option/il-select-option.props';

export interface ILOptionsListProps
{
	options?: ILSelectOptionProps[];
	onSelect?: (value: string) => void;
	onEdit?: (value: string) => void;
	onDelete?: (value: string) => void;
}
