import React from 'react';
import { ILSelectAltActionProps } from './il-select-alt-action.props';
import './il-select-alt-action.sass';

export const ILSelectAltAction = (props: ILSelectAltActionProps): React.ReactElement =>
{
  const { children, onClick } = props;

  return (<div onMouseDown={onClick} className="il-select-alt-action">{children}</div>);
};
