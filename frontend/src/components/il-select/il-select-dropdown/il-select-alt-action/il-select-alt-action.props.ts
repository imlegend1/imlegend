export interface ILSelectAltActionProps
{
	children?: string | React.ReactElement;
	onClick?: () => void | Promise<void>;
}
