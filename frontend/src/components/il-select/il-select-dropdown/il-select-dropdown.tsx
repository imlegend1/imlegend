import React from 'react';
import { ILSelectDropdownProps } from './il-select-dropdown.props';
import { ILOptionsList } from './il-options-list/il-options-list';
import { ILSelectEmpty } from './il-select-empty/il-select-empty';
import { ILSelectAltAction } from './il-select-alt-action/il-select-alt-action';
import './il-select-dropdown.sass';

export const ILSelectDropdown = (props: ILSelectDropdownProps): React.ReactElement =>
{
  const {
    noOptionsStub = 'Опции для выбора отсутствуют',
    altActionLabel,
    options,
    onChange,
    onEditOption,
    onDeleteOption,
    onAltAction,
  } = props;

  return (
    <div className="il-select-dropdown">
      {
				options!.length
				  ?
				  <ILOptionsList
				    options={options}
				    onSelect={onChange}
				    onEdit={onEditOption}
				    onDelete={onDeleteOption}
				  />
				  :
				  <ILSelectEmpty>{noOptionsStub}</ILSelectEmpty>
      }
      <ILSelectAltAction onClick={onAltAction}>{altActionLabel}</ILSelectAltAction>
    </div>
  );
};
