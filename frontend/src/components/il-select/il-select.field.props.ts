import { ILFieldProps } from '../il-field/il-field.props';
import { ILSelectProps } from './il-select.props';

export interface ILSelectFieldProps extends ILFieldProps<string>, ILSelectProps
{}
