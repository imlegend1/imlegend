import { ILSelectFieldProps } from './il-select.field.props';
import { ILFieldHOC } from '../il-field/il-field.hoc';
import { ILSelect } from './il-select';

export const ILSelectField = ILFieldHOC<ILSelectFieldProps, string>(ILSelect);
