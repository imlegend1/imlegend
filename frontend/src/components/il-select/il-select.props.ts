import { Control } from '../../../interfaces/forms/control';
import { ILShortOption } from './il-select.short-option';

export interface ILSelectProps extends Control<string>
{
	placeholder?: string;
	options?: ILShortOption[];
	/**
	 * @description Текст, который будет выводиться, если список опций пуст
	 */
	noOptionsStub?: string;
	/**
	 * @description Текст, который будет выведен ниже списка опций в блоке дополнительного действия
	 */
	altActionText?: string | React.ReactElement;
	/**
	 * @description При нажатии на иконку редактирования опции вызывается со значением опции
	 * @param value Значение целевой опции
	 */
	onEditOption?: (value: string) => void | Promise<void>;
	/**
	 * @description При нажатии на иконку удаления опции вызывается со значением опции
	 * @param value Значение целевой опции
	 */
	onDeleteOption?: (value: string) => void | Promise<void>;
	/**
	 * @description Вызывается при нажатии на блок с доп. действием
	 */
	onAltAction?: () => void | Promise<void>;
}
