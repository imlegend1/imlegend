import React, { useState } from 'react';
import { ILSelectProps } from './il-select.props';
import { ILSelectDropdown } from './il-select-dropdown/il-select-dropdown';
import { fromShort } from './helpers/from-short';
import { valueLabel } from './helpers/value-label';
import './il-select.sass';

export const ILSelect = (props: ILSelectProps): React.ReactElement =>
{
  const {
    placeholder,
    value,
    noOptionsStub = 'Опции для выбора отсутствуют',
    altActionText,
    invalid,
    options = [],
    onChange = (): void => (undefined as void),
    onBlur = (): void => (undefined as void),
    onEditOption,
    onDeleteOption,
    onAltAction,
  } = props;

  const [isShowDropdown, setShowDropdown] = useState<boolean>(false);

  let classes: string = 'il-select__control';

  const onControlBlur = (): void =>
  {
    setShowDropdown(false);
    onBlur();
  };

  if (invalid)
  {
    classes += ' il-select__control--invalid';
  }

  return (
    <div className="il-select">
      <input
        value={valueLabel(value, options)}
        placeholder={placeholder}
        onClick={(): void => setShowDropdown(!isShowDropdown)}
        onBlur={onControlBlur}
        readOnly
        className={classes}
        type="text"
      />
      {
        isShowDropdown
        &&
        <ILSelectDropdown
          noOptionsStub={noOptionsStub}
          altActionLabel={altActionText}
          options={fromShort(options, value)}
          onChange={onChange}
          onEditOption={onEditOption}
          onDeleteOption={onDeleteOption}
          onAltAction={onAltAction}
        />
      }
    </div>
  );
};
