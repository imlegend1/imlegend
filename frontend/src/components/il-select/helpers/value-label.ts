import { ILShortOption } from '../il-select.short-option';

/**
 * @description Находит описание выбранного value среди опций и возвращает его, либо возвращает сам value, если опций нет
 * @param selectedValue Выбранное пользователем значение
 * @param options Список опций для выбора
 */
export const valueLabel = (selectedValue: string = '', options: ILShortOption[] = []): string =>
{
  return options.find(({ value }: ILShortOption): boolean => value === selectedValue)?.label || '';
};
