import { valueLabel } from './value-label';
import { ILShortOption } from '../il-select.short-option';

describe('valueLabel', () =>
{
  it('should return empty string by default', () =>
  {
    expect(valueLabel()).toEqual('');
  });

  it('should return option label if it value equal first argument', () =>
  {
    const options: ILShortOption[] = [
      {value: 'MyVal', label: 'MyLabel'},
      {value: 'MyVal1', label: 'MyLabel1'},
      {value: 'MyVal2', label: 'MyLabel2'},
      {value: 'MyVal3', label: 'MyLabel3'},
    ];

    expect(valueLabel('MyVal1', options)).toEqual('MyLabel1');
  });
});
