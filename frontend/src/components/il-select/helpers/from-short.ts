import { ILSelectOptionProps } from '../il-select-dropdown/il-select-option/il-select-option.props';
import { ILShortOption } from '../il-select.short-option';

/**
 * @description Создает список опция для выбора из сокращенных вариантов
 */
export const fromShort = (src: ILShortOption[] = [], selectedValue: string = ''): ILSelectOptionProps[] =>
{
  return src.map(({ value, label}: ILShortOption): ILSelectOptionProps => ({
    value,
    children: label,
    isSelected: value === selectedValue,
  }));
};
