import { fromShort } from './from-short';
import { ILShortOption } from '../il-select.short-option';

describe('fromShort', () =>
{
  const testArray = (): ILShortOption[] => Array(10)
    .fill(null)
    .map((_: null, i: number): ILShortOption => ({ value: `value${i + 1}`, label: `Label ${i + 1}` }));

  it('should return empty array by default', () =>
  {
    expect(fromShort()).toEqual([]);
  });

  it('should return return array with same length as first argument', () =>
  {
    expect(fromShort(testArray()).length).toEqual(10);
  });

  it('should add isSelected field as true to struct with same value as second argument', () =>
  {
    expect(fromShort(testArray(), 'value3')[2].isSelected).toBeTruthy();
  });
});
