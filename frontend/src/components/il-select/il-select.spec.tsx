import React from 'react';
import { Simulate } from 'react-dom/test-utils';
import { ILSelect } from './il-select';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';
import { ILShortOption } from './il-select.short-option';
import Mock = jest.Mock;

describe('ILSelect', () =>
{
  let sandboxRenderer: SandboxRenderer;

  const select = (): HTMLElement => sandboxRenderer.container.querySelector('div.il-select') as HTMLElement;
  const control = (): HTMLInputElement => sandboxRenderer.container.querySelector('input.il-select__control') as HTMLInputElement;
  const dropdownContainer = (): HTMLElement => sandboxRenderer.container.querySelector('div.il-select-dropdown') as HTMLElement;
  const emptyOptionsText = (): HTMLElement => sandboxRenderer.container.querySelector('div.il-select-empty') as HTMLElement;
  const selectOptions = (): NodeListOf<HTMLElement> => sandboxRenderer.container.querySelectorAll('div.il-select-option');

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;
  });

  it('should render div with select class', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect/>);

    expect(select()).not.toBeNull();
  });

  it('should render readonly input control with props placeholder and same class inside div', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect placeholder="MyPlaceholder"/>);

    expect(control()).not.toBeNull();
    expect(control().readOnly).toBeTruthy();
    expect(control().placeholder).toEqual('MyPlaceholder');
  });

  it('should render input with option label if component value equal option value', () =>
  {
    const options: ILShortOption[] = [
      {value: 'MyValue', label: 'MyLabel'},
      {value: 'MyValue1', label: 'MyLabel1'},
    ];

    sandboxRenderer = new SandboxRenderer(<ILSelect value="MyValue" options={options}/>);

    expect(control().value).toEqual('MyLabel');
  });

  it('should render empty options container after click on input if has no options', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect options={[]}/>);

    expect(dropdownContainer()).toBeNull();

    Simulate.click(control());

    expect(dropdownContainer()).not.toBeNull();
  });

  it('should hide options container after double click on input', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect options={[]}/>);

    Simulate.click(control());
    Simulate.click(control());

    expect(dropdownContainer()).toBeNull();
  });

  it('should hide options container after blur on input', () =>
  {
    const options: ILShortOption[] = [];
    sandboxRenderer = new SandboxRenderer(<ILSelect options={options}/>);

    Simulate.click(control());
    Simulate.blur(control());

    expect(dropdownContainer()).toBeNull();
  });

  it('should render span text inside container if has no options', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect options={[]} noOptionsStub="No options for choose"/>);

    Simulate.click(control());

    expect(emptyOptionsText()).not.toBeNull();
    expect(emptyOptionsText().innerHTML).toEqual('No options for choose');
  });

  it('should render hardcode text inside options container by default', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect options={[]}/>);

    Simulate.click(control());

    expect(emptyOptionsText()).not.toBeNull();
    expect(emptyOptionsText().innerHTML).toEqual('Опции для выбора отсутствуют');
  });

  it('should render each option in list', () =>
  {
    const options: ILShortOption[] = Array(5)
      .fill(null)
      .map((_: null, i: number): ILShortOption => ({ value: `value${i + 1}`, label: `label ${i + 1}` }));

    sandboxRenderer = new SandboxRenderer(<ILSelect options={options}/>);

    Simulate.click(control());

    expect(selectOptions().length).toEqual(5);
    expect(selectOptions()[0].innerHTML).toMatch('label 1');
    expect(selectOptions()[1].innerHTML).toMatch('label 2');
    expect(selectOptions()[2].innerHTML).toMatch('label 3');
    expect(selectOptions()[3].innerHTML).toMatch('label 4');
    expect(selectOptions()[4].innerHTML).toMatch('label 5');
  });

  it('should output value of clicked option', () =>
  {
    const spy: Mock = jest.fn();
    const options: ILShortOption[] = Array(5)
      .fill(null)
      .map((_: null, i: number): ILShortOption => ({ value: `value${i + 1}`, label: `label ${i + 1}` }));

    sandboxRenderer = new SandboxRenderer(<ILSelect options={options} onChange={spy} />);

    Simulate.click(control());
    Simulate.mouseDown(selectOptions()[2]);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('value3');
  });

  it('should render option as selected if it has same value as component', () =>
  {
    const options: ILShortOption[] = Array(5)
      .fill(null)
      .map((_: null, i: number): ILShortOption => ({ value: `value${i + 1}`, label: `label ${i + 1}` }));

    sandboxRenderer = new SandboxRenderer(<ILSelect options={options} value="value1" />);

    Simulate.click(control());

    expect(selectOptions()[0].className).toMatch('il-select-option--selected');
  });

  it('should call edit action with same option value after click on option div', () =>
  {
    const spy: Mock = jest.fn();
    const options: ILShortOption[] = Array(5)
      .fill(null)
      .map((_: null, i: number): ILShortOption => ({ value: `value${i + 1}`, label: `label ${i + 1}` }));

    sandboxRenderer = new SandboxRenderer(<ILSelect options={options} onEditOption={spy} />);

    Simulate.click(control());
    Simulate.mouseDown(selectOptions()[2].querySelector('div.il-select-option__edit-action') as HTMLElement);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('value3');
  });

  it('should call delete action with same option value after click on option div', () =>
  {
    const spy: Mock = jest.fn();
    const options: ILShortOption[] = Array(5)
      .fill(null)
      .map((_: null, i: number): ILShortOption => ({ value: `value${i + 1}`, label: `label ${i + 1}` }));

    sandboxRenderer = new SandboxRenderer(<ILSelect options={options} onDeleteOption={spy} />);

    Simulate.click(control());
    Simulate.mouseDown(selectOptions()[2].querySelector('div.il-select-option__delete-action') as HTMLElement);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith('value3');
  });

  it('should render div with alt action by default', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect />);

    Simulate.click(control());

    expect(select().querySelector('div.il-select-alt-action')).not.toBeNull();
  });

  it('should render alt action text if defined same props', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect altActionText="Alt Action" />);

    Simulate.click(control());

    expect(select().querySelector('div.il-select-alt-action')?.innerHTML).toMatch('Alt Action');
  });

  it('should call alt action callback after click on same div', () =>
  {
    const spy: Mock = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILSelect onAltAction={spy} />);

    Simulate.click(control());
    Simulate.mouseDown(select().querySelector('div.il-select-alt-action') as HTMLElement);

    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should add invalid class if component rendered as invalid', () =>
  {
    sandboxRenderer = new SandboxRenderer(<ILSelect invalid />);

    expect(control().className).toMatch('il-select__control--invalid');
  });

  it('should call onBlur', () =>
  {
    const spy: Mock = jest.fn();

    sandboxRenderer = new SandboxRenderer(<ILSelect onBlur={spy} />);

    Simulate.blur(control());

    expect(spy).toHaveBeenCalledTimes(1);
  });
});
