/**
 * @description Сокращенная версия опции для проброса в компонент select
 */
export type ILShortOption = {
	value: string;
	label: string
}
