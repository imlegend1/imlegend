import React from 'react';
import { AuthGuardedRouteProps } from './auth-guarded-route.props';
import { GuardedRoute } from '../guarded-route/guarded-route';
import authStore from '../../mobx/stores/auth-store';

/**
 * @description В этот компонент нужно оборачивать контент, который предназначен только для авторизованных пользователей
 */
export const AuthGuardedRoute = ({ children }: AuthGuardedRouteProps): React.ReactElement =>
{
  return (
    <GuardedRoute
      redirectOnFail="/"
      canRender={() => authStore.isAuthorized}
    >
      {children}
    </GuardedRoute>
  );
};
