export interface AuthGuardedRouteProps
{
  /**
   * @description React код, который может быть отрендерен только если пользователь авторизован
   */
  children: React.ReactElement;
}
