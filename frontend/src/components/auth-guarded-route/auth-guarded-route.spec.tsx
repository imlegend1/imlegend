import React from 'react';
import * as routerModule from 'next/router';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';
import { Maybe } from '../../../interfaces/structs/maybe';
import { AuthGuardedRoute } from './auth-guarded-route';
import authStore from '../../mobx/stores/auth-store';

jest.mock('next/router');

describe('GuardedRoute', (): void =>
{
  let sandboxRenderer: SandboxRenderer;

  class RouterClass
  {
    push(): void
    {}
  }

  const ABlock = (): React.ReactElement => (<div id="A">A</div>);

  const A = (): Maybe<HTMLDivElement> => sandboxRenderer.container.querySelector('#A') as HTMLDivElement;

  beforeEach((): void =>
  {
    (routerModule.useRouter as unknown as jest.Mock).mockReturnValue(new RouterClass());
    authStore['user'] = { login: '' };
    jest.spyOn(RouterClass.prototype, 'push');
  });

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;

    jest.resetAllMocks();
  });

  it('should render component is user is authorized', (): void =>
  {
    authStore.user.login = 'MyLogin';
    expect(authStore.isAuthorized).toBeTruthy();

    sandboxRenderer = new SandboxRenderer(
      <AuthGuardedRoute>
        <ABlock />
      </AuthGuardedRoute>
    );

    expect(A()).not.toBeNull();
  });

  it('should redirect on auth if user is not authorized', (): void =>
  {
    expect(authStore.isAuthorized).toBeFalsy();

    sandboxRenderer = new SandboxRenderer(
      <AuthGuardedRoute>
        <ABlock />
      </AuthGuardedRoute>
    );

    expect(A()).toBeNull();
    expect(RouterClass.prototype.push).toHaveBeenCalledTimes(1);
    expect(RouterClass.prototype.push).toHaveBeenCalledWith('/auth');
  });
});
