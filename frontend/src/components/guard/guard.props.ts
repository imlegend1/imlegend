import React from 'react';

export interface GuardProps
{
  /**
   * @description Проверочное условие
   */
  condition: () => boolean;
  /**
   * @description React компонент, который рендерится при await condition() === true;
   */
  success: React.ReactElement;
  /**
   * @description React компонент, который рендерится при await condition() === false;
   */
  fail: React.ReactElement;
}
