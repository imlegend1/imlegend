import React from 'react';
import { act } from 'react-dom/test-utils';
import { SandboxRenderer } from '../../../test-utils/sandbox-renderer';
import { Guard } from './guard';
import { Maybe } from '../../../interfaces/structs/maybe';

describe('Guard', (): void =>
{
  let sandboxRenderer: SandboxRenderer;

  const ABlock = (): React.ReactElement => (<div id="A">A</div>);
  const BBlock = (): React.ReactElement => (<div id="B">B</div>);

  const A = (): Maybe<HTMLDivElement> => sandboxRenderer.container.querySelector('#A') as HTMLDivElement;
  const B = (): Maybe<HTMLDivElement> => sandboxRenderer.container.querySelector('#B') as HTMLDivElement;

  beforeEach((): void =>
  {
    jest.useFakeTimers();
  });

  afterEach(() =>
  {
    sandboxRenderer?.demolite();
    sandboxRenderer = undefined as unknown as SandboxRenderer;

    jest.useRealTimers();
  });

  it('should render only A if condition return true', (): void =>
  {
    const condition = (): boolean => true;

    act((): void =>
    {
      sandboxRenderer = new SandboxRenderer(
        <Guard
          condition={condition}
          success={<ABlock/>}
          fail={<BBlock/>}
        />
      );
    });

    expect(A()).not.toBeNull();
    expect(B()).toBeNull();
  });

  it('should render only B if condition return false', (): void =>
  {
    const condition = (): boolean => false;

    act((): void =>
    {
      sandboxRenderer = new SandboxRenderer(
        <Guard
          condition={condition}
          success={<ABlock/>}
          fail={<BBlock/>}
        />
      );
    });

    expect(A()).toBeNull();
    expect(B()).not.toBeNull();
  });
});
