import React from 'react';
import { GuardProps } from './guard.props';

/**
 * @description Компонент, задача которого рендерить элемент в зависимости от некоторого условия
 * @todo Сделать поддержку асинхронных условий, тк в текущем виде компонент можно заменить тернарником
 */
export const Guard = (props: GuardProps): React.ReactElement =>
{
	const { condition, success, fail } = props;

	return condition() ? success : fail;
};
