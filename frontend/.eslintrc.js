module.exports = {
  extends: [
    'airbnb-typescript-prettier',
    'plugin:react-hooks/recommended',
  ],
  env: {
    browser: true,
    jest: true,
  },
  rules: {
    'jsx-a11y/click-events-have-key-events': 'off',
    'react-hooks/rules-of-hooks': 'off',
    'jsx-a11y/aria-role': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    'react/jsx-props-no-spreading': 'off',
    '@typescript-eslint/ban-ts-comment': 'off',
    'prefer-promise-reject-errors': 'off',
    'dot-notation': 'off',
    'prettier/prettier': 'off',
    '@typescript-eslint/no-shadow': 'off',
    '@typescript-eslint/no-explicit-any': 'error',
    '@typescript-eslint/no-inferrable-types': 'off',
    'indent': ['error', 2],
    'brace-style': 'off',
    'semi': ['error', 'always'],
    '@typescript-eslint/typedef': [
      'error',
      {
        'arrowParameter': true,
        'variableDeclarationIgnoreFunction': true
      }
    ],
    '@typescript-eslint/brace-style': ['error', 'allman'],
    '@typescript-eslint/explicit-function-return-type': 'error',
    '@typescript-eslint/explicit-member-accessibility': ['error', {
      'accessibility': 'explicit',
      'overrides': {
        'accessors': 'explicit',
        'constructors': 'no-public',
        'methods': 'explicit',
        'properties': 'explicit',
        'parameterProperties': 'explicit'
      }
    }],
    'import/prefer-default-export': 'off',
    'class-methods-use-this': 'off',
    'no-useless-constructor': 'off',
    'import/no-extraneous-dependencies': ['error', {'devDependencies': true}],
    'import/no-unresolved': 'off',
  },
};
