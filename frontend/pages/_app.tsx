import React from 'react';
import type { AppProps } from 'next/app';
import './_app.sass';

const App = (props: AppProps): React.ReactElement => 
{
  const { Component, pageProps } = props;

  return <Component {...pageProps} />;
};

export default App;
