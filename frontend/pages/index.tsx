import React from 'react';
import './index.sass';
import { Form, Formik } from 'formik';
import { observer } from 'mobx-react';
import Link from 'next/link';
import { NextRouter, useRouter } from 'next/router';
import { ILInputField } from '../src/components/il-input/il-input.field';
import { ILShortOption } from '../src/components/il-select/il-select.short-option';
import { ILSelectField } from '../src/components/il-select/il-select.field';
import { ILButton } from '../src/components/il-button/il-button';
import { ILDatepickerField } from '../src/components/il-datepicker/il-datepicker.field';
import rootStore from '../src/mobx/stores/root-store';
import { useStores } from '../src/mobx/hooks/use-stores';

type RootStore = typeof rootStore;

const Index = observer<React.FC>((): React.ReactElement =>
{
  const { authStore }: RootStore = useStores();
  const router: NextRouter = useRouter();

  return (
    <div className="index-block">
      <h1>User name is: {authStore.user?.login || 'Unknown'}</h1>
      <ILButton onClick={() => authStore.authenticate()}>Sign in</ILButton>
      <div style={{width: 300}}>
        <Formik
          initialValues={{test: 'Abibasta123', testSelect: '1', testDate: new Date()}}
          validate={(_: { test: string }): object => ({ test: 'Error!' } as object)}
          validateOnBlur
          validateOnMount
          onSubmit={(): void => undefined}
        >
          {
            (): React.ReactElement => (
              <Form>
                <ILInputField name="test" />
                <ILSelectField
                  options={Array(10).fill(null).map((_: null, i: number): ILShortOption => (
                    {
                      label: `Option ${i}`,
                      value: String(i),
                    }
                  ))}
                  name="testSelect"
                />
                <ILDatepickerField name="testDate" />
                <ILButton onClick={() => router.push('/about') as unknown as void}>Submit</ILButton>
              </Form>
            )
          }
        </Formik>
      </div>
      <Link href="/about"><a>To About Page!!!</a></Link>
    </div>
  );
});

export default Index;
