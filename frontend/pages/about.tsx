import React from 'react';
import { AuthGuardedRoute } from '../src/components/auth-guarded-route/auth-guarded-route';

const About = (): React.ReactElement => (
	<AuthGuardedRoute>
		<div>About!!!</div>
	</AuthGuardedRoute>
);

export default About;
