export class Benchmark
{
	private start_: number = undefined as unknown as number;
	private end_: number = undefined as unknown as number;

	/**
	 * @description Проверяет, начался ли отсчет времени выполнения какой-либо процедуры
	 */
	private isStartMarked(): boolean
	{
		return (this.start_ !== undefined);
	}

	/**
	 * @description Проверяет, завершился ли отсчет времени выполнения какой либо процедуры
	 */
	private isEndMarked(): boolean
	{
		return (this.end_ !== undefined);
	}

	/**
	 * @description Начинает отсчет времени выполнения процедуры
	 */
	public start(): void
	{
		if (!this.isStartMarked())
		{
			this.start_ = performance.now();
		}
		else
		{
			throw new Error('Start can be assigned only one time to one benchmark object.');
		}
	}

	/**
	 * @description Завершает отсчет времени выполнения процедуры
	 */
	public end(): void
	{
		if (!this.isEndMarked() && this.isStartMarked())
		{
			this.end_ = performance.now();
		}
		else if (!this.isStartMarked())
		{
			throw new Error('You must set the start!');
		}
		else
		{
			throw new Error('End can be assigned only one time to one benchmark object.');
		}
	}

	/**
	 * @description После завершения отсчета возвращает время(в миллисекундах) между началом и концом отсчета
	 */
	public time(): number
	{
		if (this.isStartMarked() && this.isEndMarked())
		{
			return (this.end_ - this.start_);
		}
		else
		{
			throw new Error('start or end is not measured in this instance.');
		}
	}
}
