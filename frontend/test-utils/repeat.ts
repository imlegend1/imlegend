/**
 * @description Повторяет одну и ту же операцию множество раз для вычисления наиболее точной производительности в реальных условиях
 * @param action Целевая операция
 */
export const repeat = (action: (iteration: number, max: number) => void): void =>
{
	for (let i = 0; i < 10000; i++)
	{
		action(i + 1, 10000);
	}
};
