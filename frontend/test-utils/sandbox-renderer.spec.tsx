import React from  'react';
import { SandboxRenderer } from './sandbox-renderer';

describe('Test Utils | SandboxRenderer', () =>
{
	let sandboxRenderer: SandboxRenderer;
	const TestComponent = () => (<div id="TestID">Test Component!</div>);

	afterEach(() =>
	{
		sandboxRenderer.demolite();
	});

	it('should add one inner div to document.body element', () =>
	{
		sandboxRenderer = new SandboxRenderer(<TestComponent/>);

		expect(document.body.children.length).toEqual(1);
		expect(document.body.children[0].tagName).toEqual('DIV');
	});

	it('should render component in DOM after instance creation', () =>
	{
		sandboxRenderer = new SandboxRenderer(<TestComponent/>);

		expect(document.body.querySelector('#TestID')).not.toBeNull();
	});

	describe('SandboxRenderer | demolite', () =>
	{
		it('should leave empty document.body after call', () =>
		{
			sandboxRenderer = new SandboxRenderer(<TestComponent/>);

			sandboxRenderer.demolite();
			expect(document.body.children.length).toEqual(0);
		});
	});

	describe('SandboxRenderer | container', () =>
	{
		it('should return element ref, which contain tested layout', () =>
		{
			sandboxRenderer = new SandboxRenderer(<TestComponent/>);

			expect(sandboxRenderer.container).not.toBeNull();
			expect(sandboxRenderer.container.innerHTML).toEqual('<div id="TestID">Test Component!</div>');
		});
	});
});
