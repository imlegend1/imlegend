import { repeat } from './repeat';

describe('Test Utils | repeat', () =>
{
	it('should call some action 10000 times by default', () =>
	{
		const action: jest.Mock = jest.fn();

		repeat(action);

		expect(action).toHaveBeenCalledTimes(10000);
	});

	it('should call every action with number of iteration from 1', () =>
	{
		const action: jest.Mock = jest.fn();

		repeat(action);

		expect(action).toHaveBeenNthCalledWith(1, 1, expect.any(Number));
	});

	it('should call every action with count of iterations', () =>
	{
		const action: jest.Mock = jest.fn();

		repeat(action);

		expect(action).toHaveBeenNthCalledWith(1, expect.any(Number), 10000);
	});
});
