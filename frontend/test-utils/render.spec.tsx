import React from 'react';
import ReactDOM from 'react-dom';
import { render } from './render';
import { Benchmark } from './benchmark';
import { Benchmarks } from './benchmark.constants';
import { repeat } from './repeat';

describe('Test Utils | render', () =>
{
	let container: HTMLDivElement;
	const TestComponent = (): React.ReactElement => (<div id="TestID">Renderable data</div>);

	beforeEach(() =>
	{
		container = document.createElement('div');
		jest.spyOn(ReactDOM, 'render');
	});

	afterEach(() =>
	{
		jest.restoreAllMocks();
	});

	it('should call React-DOM render on call', () =>
	{
		render(<TestComponent/>, container);

		expect(ReactDOM.render).toHaveBeenCalledTimes(1);
	});

	it('should render component template in container', () =>
	{
		render(<TestComponent/>, container);

		expect(container.querySelector('#TestID')).not.toBeNull();
	});

	it('should have at least medium performance', () =>
	{
		const benchmark: Benchmark = new Benchmark();

		benchmark.start();
		repeat(() =>
		{
			render(<TestComponent/>, container);
		});
		benchmark.end();

		expect(benchmark.time()).toBeLessThanOrEqual(Benchmarks.Medium);
	});
});
