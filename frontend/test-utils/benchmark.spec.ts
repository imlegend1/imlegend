import { Benchmark } from './benchmark';

describe('Test Utils | benchmark', () =>
{
	let benchmark: Benchmark;

	beforeEach(() =>
	{
		benchmark = new Benchmark();
	});

	it('should has number start field', () =>
	{
		expect('start_' in benchmark).toBeTruthy();
	});

	it('should has number end field', () =>
	{
		expect('end_' in benchmark).toBeTruthy();
	});

	describe('benchmark | start', () =>
	{
		it('should set start_ variable if call first', () =>
		{
			benchmark.start();

			expect(benchmark['start_']).toBeGreaterThan(0);
		});

		it('should throw error if call second', () =>
		{
			const test = () =>
			{
				benchmark.start();
				benchmark.start();
			};

			expect(test).toThrowError('Start can be assigned only one time to one benchmark object.');
		});
	});

	describe('benchmark | end', () =>
	{
		it('should set end_ variable if call first', () =>
		{
			benchmark.start();
			benchmark.end();

			expect(benchmark['end_']).toBeGreaterThan(0);
		});

		it('should throw error if call second', () =>
		{
			const test = () =>
			{
				benchmark.start();
				benchmark.end();
				benchmark.end();
			};

			expect(test).toThrowError('End can be assigned only one time to one benchmark object.');
		});

		it('should throw error if start is not defined', () =>
		{
			const test = () =>
			{
				benchmark.end();
			};

			expect(test).toThrowError('You must set the start!');
		});
	});

	describe('benchmark | time', () =>
	{
		it('should throw error by default', () =>
		{
			const test = () =>
			{
				benchmark.time();
			};

			expect(test).toThrowError('start or end is not measured in this instance.');
		});

		it('should not throw error if end and start variable are defined.', () =>
		{
			const test = () =>
			{
				benchmark.start();
				benchmark.end();
				benchmark.time();
			};

			expect(test).not.toThrow(Error);
		});

		it('should return diff between start and end after it were setted', () =>
		{
			benchmark.start();
			benchmark.end();

			expect(benchmark.time()).toEqual(benchmark['end_'] - benchmark['start_']);
		});
	});

	describe('benchmark | isStartMarked', () =>
	{
		it('should return false by default', () =>
		{
			expect(benchmark['isStartMarked']()).toBeFalsy();
		});

		it('should return true if called after start()', () =>
		{
			benchmark.start();
			expect(benchmark['isStartMarked']()).toBeTruthy();
		});
	});

	describe('benchmark | isEndMarked', () =>
	{
		it('should return false by default', () =>
		{
			expect(benchmark['isEndMarked']()).toBeFalsy();
		});

		it('should return true if called after end()', () =>
		{
			benchmark.start();
			benchmark.end();
			expect(benchmark['isEndMarked']()).toBeTruthy();
		});
	});
});
