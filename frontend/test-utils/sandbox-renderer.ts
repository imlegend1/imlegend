import React from 'react';
import { unmountComponentAtNode } from 'react-dom';
import { render } from './render';
import { act } from 'react-dom/test-utils';

export class SandboxRenderer
{
	private body: HTMLElement = document.body;
	private container_: HTMLDivElement = document.createElement('div');

	/**
	 * @description Возвращает контейнер, внутри которого рендерятся компоненты
	 */
	public get container(): HTMLDivElement
	{
		return this.container_;
	}

	constructor(reactElement: React.ReactElement)
	{
		this.body.appendChild(this.container);
		act(() =>
		{
			render(reactElement, this.container);
		});
	}

	/**
	 * @description Очищает DOM
	 */
	public demolite(): void
	{
		this.container.remove();
		unmountComponentAtNode(this.container);
	}
}
