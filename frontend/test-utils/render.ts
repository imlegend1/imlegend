import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';

/**
 * @description Создает из jsx элемента html, и кладет его внутрь другого html элемента.
 * @param element Отрендеренный компонент React
 * @param container Контейнер, в котором будет отрендерен компонент React
 */
export const render = (element: React.ReactElement, container: HTMLDivElement): void =>
{
	act(() =>
	{
		ReactDOM.render(element, container);
	});
};
