import { Maybe } from '../structs/maybe';

/**
 * @description Обобщенное представление контейнера-итератора
 */
export interface Iterable<T>
{
	readonly size: number;
	prev(): T;
	next(): T;
	end(): boolean;
	find<K>(lambda: (data: K, entity: T) => boolean): Maybe<T>;
	keys(): string[];
	values(): T[];
	entries(): [string, T][];
	[Symbol.iterator]: () => IteratorResult<T, T>;
}
