type JSONField = string | number | boolean | undefined | null | BackendJSON |
								string[] | number[] | boolean[] | undefined[] | null[] | BackendJSON[];

/**
 * @description Максимально обобщенная структура данных с бэкенда, предназначено для наследования более конкретных структур
 */
export interface BackendJSON
{
	[key: string]: JSONField;
}
