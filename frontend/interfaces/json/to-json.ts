import { BackendJSON } from './backend-json';

export interface ToJSON
{
	/**
	 * @description Конвертирует данные текущего объекта в формат для отправки на бэкенд
	 */
	toJSON(): BackendJSON;
}
