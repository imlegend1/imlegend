export interface Create<I, O>
{
	create(input: I): Promise<O>;
}
