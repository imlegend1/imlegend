export interface ReadUnique<T>
{
	/**
	 * @description Запрашивает сущность по id
	 * @param id Уникальный ключ целевой сущности
	 */
	readUnique(id: string | number): Promise<T>;
}
