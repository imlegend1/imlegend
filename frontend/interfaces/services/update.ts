export interface Update<I, O>
{
	update(input: I): Promise<O>;
}
