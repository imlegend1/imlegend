export interface Read<T>
{
	read(queryParams: Record<string, string>): Promise<T>;
}
