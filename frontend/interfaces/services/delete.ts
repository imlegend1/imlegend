export interface Delete
{
	delete(id: string | number): Promise<void>;
}
