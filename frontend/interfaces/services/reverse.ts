export interface Reverse<I, O>
{
  reverse(input: O): I;
}
