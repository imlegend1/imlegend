type TaskProgress = {
	done: number;
	start: number;
	finish: number;
};

export interface Task
{
	/**
	 * @description Делает задачу открытой
	 */
	open(): void;
	/**
	 * @description Закрывает задачу
	 */
	close(): void;
	/**
	 * @description Отмечает прогресс по задаче
	 */
	check(doneProgress: number): void;
	/**
	 * @description Если прогресс по задаче равен финишу, возвращает true
	 */
	isCompleted(): boolean;
	/**
	 * @description Возвращает структуру прогресса по задаче
	 */
	progress(): TaskProgress;
}
