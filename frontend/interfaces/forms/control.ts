/**
 * @description Базовый класс для работы с полями формы
 */
export interface Control<T>
{
	/**
	 * @description Значение, которое хранит поле
	 */
	value?: T;
	/**
	 * @description Побочный эффект, вызываемый при попытке изменения контрола.
	 */
	onChange?: (newValue: T) => void | Promise<void>;

	onBlur?: () => void | Promise<void>;
  /**
	 * @description Если true, то контрол не может изменить свое значение и вызвать onChange
	 */
	disabled?: boolean;

	invalid?: boolean;
}
